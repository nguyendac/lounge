function initMap() {
  var uluru = { lat: 34.707036, lng: 135.497878 };
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: uluru,
    panControl: false,
    scrollwheel: false,
    zoomControl: true,
    mapTypeId: 'roadmap',
    disableDefaultUI: true,
    styles: [{
        "stylers": [
          { "hue": "#858585" },
          { "saturation": -100 },
        ],
        "elementType": "all",
        "featureType": "all"
      },
      {
        "stylers": [{ "color": "#858585" }],
        "featureType": "road.highway",
      }
    ]
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map

  });
  google.maps.event.addDomListener(window, 'resize', function() {
    var center = map.getCenter()
    google.maps.event.trigger(map, "resize")
    map.setCenter(center)
  });
}

window.addEventListener('DOMContentLoaded',function(){
  new Slide();
  // $('.slick').slick({
  //   slidesToShow: 3,
  //   slidesToScroll: 3,
  //   dots: false,
  // })
  // window.addEventListener('resize',function(){
  //   if(window.innerWidth <  769) {
  //     $('.slick').slick('unslick');
  //   } else {
  //     $('.slick').slick({
  //       slidesToShow: 3,
  //       slidesToScroll: 3,
  //       dots: false,
  //     })
  //   }
  // })
  // window.addEventListener('load',function(){
  //   if(window.innerWidth <  769) {
  //     $('.slick').slick('unslick');
  //   } else {
  //     $('.slick').slick({
  //       slidesToShow: 3,
  //       slidesToScroll: 3,
  //       dots: false,
  //     })
  //   }
  // })
  var swiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      spaceBetween: 46,
      loop: true,
      loopFillGroupWithBlank: true,
      navigation: {
        nextEl: '.swiper-next',
        prevEl: '.swiper-prev',
      },
      breakpoints: {
        768: {
          slidesPerView: 1,
          loop: false,
          scrollbar: {
            el: '.swiper-scrollbar',
            draggable: false,
          },
        }
      }
    });
})
var Slide = (function(){
  function Slide(){
    var s = this;
    this.target = 'under';
    this.max_height_target;
    this.step  = 0.00035;
    this.scale = 1;
    this.currentSlide = 0;
    this.opacity = 0;
    this.timer;
    this.timeout;
    this.img;
    this.ratio_pc = 2.404;
    this.ratio_sp = 1.6;
    this.eles = document.getElementById(this.target).querySelectorAll('ul li img');
    this.func_transition = function(){
      s.img = s.eles[s.currentSlide];
      s.img.closest('li').style.opacity = 1;
      s.scale+=s.step;
      s.img.style[transformProperty] = 'scale('+s.scale+')';
      if(s.scale >= 1.15){
        s.currentSlide+=1;
        s.img.closest('li').style.opacity = 0;
        s.timeout = setTimeout(function(){
          s.img.style[transformProperty] = 'scale(1)';
        },2000);
        s.scale = 1;
        if(s.currentSlide > s.eles.length-1) {
          s.currentSlide = 0;
        }
      }
      s.timer = window.requestAnimFrame(s.func_transition);
    }
    this.sizeWindow = function(){
      Array.prototype.forEach.call(s.eles,function(el,i){
        el.closest('li').style.opacity = s.opacity;
        var img = new Image();
        img.onload = function(){
          s.getRatio(img);
        }
        img.src = el.getAttribute('src');
      });
    }
    this.getRatio = function(img){
      var ratio;
      ratio = img.naturalWidth/img.naturalHeight;
      return ratio;
    }
    window.addEventListener('resize',function(){
      s.sizeWindow();
    })
    window.addEventListener('load',function(){
      s.sizeWindow();
    })
    this.sizeWindow();
    this.func_transition();
  }
  return Slide;
})()
