window.addEventListener('DOMContentLoaded',function(){
  var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 8,
      slidesPerView: 10,
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      navigation: {
        nextEl: '.swiper-control-next',
        prevEl: '.swiper-control-prev',
      },
      breakpoints: {
        769: {
          slidesPerView: 4,
          freeMode: false,
          watchSlidesVisibility: false,
          watchSlidesProgress: false,
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
          }
        }
      }
    });
  var galleryTop = new Swiper('.gallery-top', {
      // spaceBetween: 10,
      autoHeight: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs
      },
      pagination: {
        el: '.swiper-pagi',
        type: 'fraction',
        formatFractionCurrent: function (number) {
          if(number < 10) {
            return '0'+number;
          } else {
            return number;
          }
        },
      },

    });
})