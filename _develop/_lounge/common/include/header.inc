<div class="bx_header">
  <div class="row wrap">
    <h1 class="bx_header_logo"><a href="#"><img src="/common/images/logo.png" alt="Logo"></a></h1>
    <div class="bx_header_r">
      <nav class="nav" id="nav">
        <a id="close_nav" href="" class="close show_sp">close</a>
        <ul>
          <li><a href="#">アバウト</a></li>
          <li><a href="#">VisionsWay</a></li>
          <li><a href="#">トピックス</a></li>
          <li><a href="#">スペースレンタル</a></li>
          <li><a href="#">お問い合わせ</a></li>
        </ul>
        <ul class="show_sp">
          <li class="visionway"><a href="">VisionsWay一期生募集中</a></li>
          <li class="login"><a href="#"><span>ログイン</span></a></li>
        </ul>
      </nav>
      <!--/.nav-->
      <ul class="btn_link">
        <li class="schedule"><a href="#" data-content="SCHEDULE"><span>スケジュール</span></a></li>
        <li class="access"><a href="#" data-content="ACCESS"><span>アクセス</span></a></li>
        <li class="login show_pc" id="login"><a href="#"><span>ログイン</span></a></li>
      </ul>
      <div class="bx_header_r_open show_sp" id="open_nav" data-content="MENU"><span>open</span></div>
    </div>
    <!--/.right-->
  </div>
  <!--/.wrap-->
</div>
<!--/.bx_header-->
