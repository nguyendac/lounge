<div class="bx_footer">
  <div class="bx_footer_top">
    <div class="row wrap">
      <h2 class="logo_ft show_pc"><a href=""><img src="/common/images/logo_ft.png" alt="Logo footer"></a></h2>
      <ul class="nav_ft">
        <li><a href="#">アバウト</a></li>
        <li><a href="#">Visions Way</a></li>
        <li><a href="#">トピックス</a></li>
        <li><a href="#">ニュース</a></li>
        <li><a href="#">スペースレンタル</a></li>
        <li><a href="#">利用料金・利用規約</a></li>
        <li><a href="#">パートナー企業</a></li>
        <li><a href="#">パートナーご希望の<br class="show_sp">企業様へ</a></li>
        <li><a href="#">イベント&スケジュール</a></li>
        <li><a href="#">アクセス</a></li>
        <li><a href="#">プライバシーポリシー</a></li>
        <li><a href="#">お問い合わせ</a></li>
        <li class="show_pc"><a href="#">ログイン</a></li>
      </ul>
      <!--/.nav_ft-->
    </div>
    <!--/.wrap-->
  </div>
  <!--/.top-->
  <div class="bx_footer_bottom">
    <h2 class="logo_ft show_sp"><a href=""><img src="/common/images/logo_ft.png" alt="Logo footer"></a></h2>
    <ul class="list_social">
      <li><a href="#"><img src="/common/images/icon_fb.png" alt="Icon Facebook"></a></li>
      <li><a href="#"><img src="/common/images/icon_tw.png" alt="Icon Tweeter"></a></li>
      <li><a href="#"><img src="/common/images/icon_int.png" alt="Icon Intagram"></a></li>
      <li><a href="#"><img src="/common/images/icon_b.png" alt="Icon B"></a></li>
      <li><a href="#"><img src="/common/images/icon_line.png" alt="Icon Line"></a></li>
    </ul>
    <!--/.list_social-->
    <p class="copyright">Copyright &copy;  Visions LOUNGE 2018</p>
  </div>
  <!--/.bottom-->
</div>
<!--/.bx_footer-->