window.addEventListener('DOMContentLoaded', function() {
  new typeActive();
  new Tab();
})
var Tab = (function() {
  function Tab() {
    var t = this;
    this._target = document.getElementById('according');
    this._eles = this._target.querySelectorAll('a');
    Array.prototype.forEach.call(t._eles, function(el) {
      el.addEventListener('click', function(e) {
        this.parentNode.classList.add('active');
        Array.prototype.forEach.call(t._eles, function(child) {
          if (child != el) {
            child.parentNode.classList.remove('active');
          }
        });
      })
    })
    this._target.addEventListener('click', function() {
      if (this.classList.contains('active')) {
        this.classList.remove('active');
      } else {
        this.classList.add('active');
      }
    });
  }
  return Tab;
})()

var typeActive = (function() {
  function typeActive() {
    var m = this;
    this._type = parseInt(window.location.href.split('?type=')[1]) ? parseInt(window.location.href.split('?type=')[1]) : 0;
    this.curr_type = this._type;
    // console.log(this._type);
    this._target = document.getElementById('according');
    this.eles = this._target.querySelectorAll('li');
    Array.prototype.forEach.call(this.eles, function(a) {
      if (a.dataset.type == m._type) {
        if (a.classList.contains('active')) {
          a.classList.remove('active');
        } else {
          a.classList.add('active');
        }
      }
    });
  }
  return typeActive;
})()