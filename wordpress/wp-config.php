<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lounge');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',hzeM+L:|spNd< >>ZEa[uqX!c:j+53rTGIpf1!|hS`X>($DgV=XK4)70@u=Xafv');
define('SECURE_AUTH_KEY',  '$[Q@p^aFaD{3~ QWMRX:Ku`^RKM+axy+ZP5kAH^*vgv_0[pqQ-{31An: :NUctrq');
define('LOGGED_IN_KEY',    '[jq7(uNo]0XQxR<H!GhVz8irAz*xrQG7?:A$|R_9ev|Y#Zi_J8F<Y6qLgIo2}[Z+');
define('NONCE_KEY',        'H*+VNY~~Jho5cs5wq7|fi?wZ`r`h~LFn&whM W,=Tk3Mc4YK(Lnw6rE]wyuJFB$3');
define('AUTH_SALT',        '_gmRwIASy%S(2G%3KA00=#jj_{@_=T-fA<I/7Q9_t}(n$*k[Y5*]=?.?psYDNu25');
define('SECURE_AUTH_SALT', 'ZpI@_Wcm}:[^Xi4Shq+H3OL%k8FQz9&-l77tRb(f` HU>6PRM5[(pOg7.aGDMb+-');
define('LOGGED_IN_SALT',   'imFeWSGxrc}g7!]E~p-QGk:k:@gXvVq3{&6b!=BI6%5_gAZnphJQ>  @iK]k;I3l');
define('NONCE_SALT',       'pHc8O`1U{^:HTH(E<4 bfyy6]qV|B`hK;aKlXwn sq@$6vTr7j%(T>!Ha`i55fT]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
