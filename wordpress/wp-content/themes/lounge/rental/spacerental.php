<?php
/*
  Template Name: Space Rental Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_rental">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/rental/images/bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/rental/images/bkg_ttl_pc.jpg" alt="rental 01" />
            </picture>
            <h2>SPACE<br class="show_sp"> RENTAL<span>スペースレンタル</span></h2>
          </div>
        </div>
        <!--/.ttl-->
        <div class="gr_rental">
          <div class="row wrap">
            <p class="txt_top">
              Visions LOUNGEは梅田駅徒歩０分。<br>
              木目を基調とした中に緑がある温かい空間。全面ガラス張りで梅田の街を見渡せます。<br class="show_pc">
              企業向け、学生団体向けのレンタルもおこなっておりますのでお気軽にお問い合わせください。<br>
              貸出可能なスペースは３種類。用途に合わせて様々なシーンでお使いいただけます。
            </p>
            <div class="bx_center">
              <div class="bx_center_wrap">
                <picture class="effect maskToRight">
                  <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/rental/images/img_top_sp.png" />
                  <img src="<?php bloginfo('template_url')?>/rental/images/img_top_pc.jpg" alt="rental 01" />
                </picture>
              </div>
              <div class="ctn">
                <div class="ctn_left">
                  <h3 class="ttl_art">全面スペース</h3>
                  <p>セミナー、イベント、説明会等幅広くご活用いただけます。<br class="show_pc">
                  また、各種テーブルや椅子を自由にレイアウトいただけます<br class="show_pc"></p>
                </div>
                <!--/.left-->
                <div class="ctn_right">
                  <ul class="list_p">
                    <li><span>広さ・収容人数</span><em>85㎡　最大席数56席</em></li>
                    <li><span>料金</span><em>18000円/1h(税別)</em></li>
                  </ul>
                  <!--/.list_p-->
                </div>
                <!--/.right-->
              </div>
            </div>
            <!--/.bx_center-->
            <div class="bx_art">
              <article>
                <div class="art_top">
                  <div class="art_top_wrap">
                    <picture class="effect maskToRight">
                      <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/rental/images/img_s_01_sp.png" />
                      <img src="<?php bloginfo('template_url')?>/rental/images/img_s_01_pc.jpg" alt="rental 01" />
                    </picture>
                  </div>
                  <h3 class="ttl_art">半面スペース</h3>
                  <p>可動式の間仕切りを使って出来上がるスペース。小規模のセミナー、イベント等にご活用いただけます。また、各種テーブルや椅子を自由にレイアウトいただけます。</p>
                </div>
                <!--/.art_top-->
                <ul class="list_p">
                  <li><span>広さ・収容人数</span><em>30㎡　最大25名程度</em></li>
                  <li><span>料金</span><em>9000円/1h(税別)</em></li>
                </ul>
                <!--/.list_p-->
              </article>
              <article>
                <div class="art_top">
                  <picture class="effect maskToRight">
                    <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/rental/images/img_s_02_sp.png" />
                    <img src="<?php bloginfo('template_url')?>/rental/images/img_s_02_pc.jpg" alt="rental 01" />
                  </picture>
                  <h3 class="ttl_art">会議室</h3>
                  <p>ミーティングや打ち合わせ、少人数でのイベントにご活用いただけます。</p>
                </div>
                <!--/.art_top-->
                <ul class="list_p">
                  <li><span>広さ・収容人数</span><em>13㎡　最大8名程度</em></li>
                  <li><span>料金</span><em>6000円/1h(税別)</em></li>
                </ul>
                <!--/.list_p-->
              </article>
            </div>
            <!--/.bx_art-->
            <div class="bx_bottom">
              <h3 id="equipment">利用可能備品</h3>
              <div class="bx_bottom_wrap">
                <div class="bx_bottom_thumb">
                  <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/01.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/02.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/03.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/04.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/05.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/06.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/07.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/08.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/09.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/10.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/11.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/12.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/13.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/14.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/15.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/16.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/17.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/18.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/19.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/20.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/21.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/22.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/23.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/24.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/25.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/26.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/27.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/28.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/29.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/30.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/31.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/32.jpg" alt="spacerental"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/33.jpg" alt="spacerental"></figure>
                      </div>
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <!-- pagination -->
                    <div class="swiper-pagi"></div>
                  </div>
                  <div class="swiper-container gallery-thumbs">
                    <div class="swiper-wrapper">
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/01.jpg" alt="spacerental 01"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/02.jpg" alt="spacerental 02"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/03.jpg" alt="spacerental 03"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/04.jpg" alt="spacerental 04"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/05.jpg" alt="spacerental 05"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/06.jpg" alt="spacerental 06"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/07.jpg" alt="spacerental 07"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/08.jpg" alt="spacerental 08"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/09.jpg" alt="spacerental 09"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/10.jpg" alt="spacerental 10"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/11.jpg" alt="spacerental 11"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/12.jpg" alt="spacerental 12"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/13.jpg" alt="spacerental 13"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/14.jpg" alt="spacerental 14"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/15.jpg" alt="spacerental 15"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/16.jpg" alt="spacerental 16"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/17.jpg" alt="spacerental 17"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/18.jpg" alt="spacerental 18"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/19.jpg" alt="spacerental 19"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/20.jpg" alt="spacerental 20"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/21.jpg" alt="spacerental 21"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/22.jpg" alt="spacerental 22"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/23.jpg" alt="spacerental 23"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/24.jpg" alt="spacerental 24"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/25.jpg" alt="spacerental 25"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/26.jpg" alt="spacerental 26"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/27.jpg" alt="spacerental 27"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/28.jpg" alt="spacerental 28"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/29.jpg" alt="spacerental 29"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/30.jpg" alt="spacerental 30"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/31.jpg" alt="spacerental 31"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/32.jpg" alt="spacerental 32"></figure>
                      </div>
                      <div class="swiper-slide">
                        <figure><img src="<?php bloginfo('template_url')?>/rental/images/33.jpg" alt="spacerental 33"></figure>
                      </div>
                    </div>

                  </div>
                  <div class="swiper-control-next"></div>
                  <div class="swiper-control-prev"></div>
                  <!-- pagination -->
                  <div class="swiper-pagination"></div>
                </div>
                <dl>
                  <dt>大規模・中規模スペース</dt>
                  <dd>
                    <ul>
                      <li>・プロジェクター(HDMI端子) </li>
                      <li>・Mac接続用アダプタ</li>
                      <li>・スクリーン(縦180cm×横230cm)</li>
                      <li>・無線マイク×2</li>
                      <li>・スピーカー(iphone接続可)</li>
                      <li>・丸テーブル(2名がけ)×5</li>
                      <li>・ソファ用テーブル(2名がけ)×4 </li>
                      <li>・角テーブル(4名がけ)×3 </li>
                      <li>・長テーブル(8名がけ)×2 </li>
                      <li>・ダイニングチェア×34 </li>
                      <li>・一人がけソファ×8 </li>
                      <li>・スツール×10</li>
                      <li>・プラスチックチェア×4</li>
                      <li>・Body Make Seay Style×33</li>
                      <li>・両面ホワイトボード×2</li>
                      <li>・ホワイトボードペン</li>
                      <li>・入り口用案内ボード</li>
                      <li>・延長コード</li>
                    </ul>
                  </dd>
                </dl>
                <dl>
                  <dt>小規模スペース</dt>
                  <dd>
                    <ul>
                      <li>・角テーブル（4名がけ）×2</li>
                      <li>・パイプ椅子×8</li>
                      <li>・プロジェクター(HDMI端子)</li>
                      <li>・延長コード</li>
                    </ul>
                  </dd>
                </dl>
              </div>
            </div>
            <!--/.bx_bottom-->
          </div>
        </div>
        <!--/.gr_rental-->
      </section>
      <!--/.st_rental-->
      <section class="st_flow">
        <div class="row wrap">
          <h2 class="ttl_flow">FLOW</h2>
          <ul>
            <li class="effect fadeIn delay_03">
              <span class="show_pc">STEP.01</span>
              <figure><img src="<?php bloginfo('template_url')?>/rental/images/icon_01.png" alt="icon 01"></figure>
              <div class="txt_em">
                <span class="show_sp">STEP.01</span>
                <em>
                  <strong>空き状況を確認</strong>
                  カレンダーにて空き状況をご確認ください
                </em>
              </div>
            </li>
            <li class="effect fadeIn delay_03">
              <span class="show_pc">STEP.02</span>
              <figure><img src="<?php bloginfo('template_url')?>/rental/images/icon_02.png" alt="icon 02"></figure>
              <div class="txt_em">
                <span class="show_sp">STEP.02</span>
                <em>
                  <strong>予約リクエスト</strong>
                  問い合わせフォームより予約希望のご連絡をお願いいたします<br>（ご利用備品・その他ご希望等についてもこの際にお伝えください）
                </em>
              </div>
            </li>
            <li class="effect fadeIn delay_03">
              <span class="show_pc">STEP.03</span>
              <figure><img src="<?php bloginfo('template_url')?>/rental/images/icon_03.png" alt="icon 03"></figure>
              <div class="txt_em">
                <span class="show_sp">STEP.03</span>
                <em>
                  <strong>予約可否を確認</strong>
                  担当者が内容を確認し、申込書をお送りさせていただきます
                </em>
              </div>
            </li>
            <li class="effect fadeIn delay_03">
              <span class="show_pc">STEP.04</span>
              <figure><img src="<?php bloginfo('template_url')?>/rental/images/icon_04.png" alt="icon 04"></figure>
              <div class="txt_em">
                <span class="show_sp">STEP.04</span>
                <em>
                  <strong>予約確定</strong>
                  メールにて申込書を返送いただきご予約確定となります
                </em>
              </div>
            </li>
            <li class="effect fadeIn delay_03">
              <span class="show_pc">STEP.05</span>
              <figure><img src="<?php bloginfo('template_url')?>/rental/images/icon_05.png" alt="icon 05"></figure>
              <div class="txt_em">
                <span class="show_sp">STEP.05</span>
                <em>
                  <strong>お支払い</strong>
                  お支払いは請求書払いのみとなります（月末締め翌月末支払い）
                </em>
              </div>
            </li>
          </ul>
        </div>
      </section>
      <!--/.st_flow-->
      <section class="st_cld">
        <div class="row wrap">
          <h2 class="ttl_cld">レンタル予約空き状況</h2>
          <div class="cld">
            <?php
              $wrap_calendar = get_bloginfo('template_url').'/gcalendar-wrapper.php';
            ?>
            <iframe src="<?php _e($wrap_calendar) ?>?title=Visions%20LOUNGE&amp;showTitle=0&amp;showDate=0&amp;showPrint=0&amp;showCalendars=0&amp;showTz=0&amp;mode=WEEK&amp;height=600&amp;wkst=2&amp;hl=ja&amp;bgcolor=%23FFFFFF&amp;src=8lm9iqij1u4109tc7lhk2qloac%40group.calendar.google.com&amp;color=%232F6309&amp;src=gmqsiv0ab5fldl3nfis6untu7s%40group.calendar.google.com&amp;color=%23AB8B00&amp;ctz=Asia%2FTokyo" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
            <p class="explain"><span class="blue">ラウンジスペースの予定（営業時間・イベント・貸切）</span><span class="yellow">会議室の予定</span></p>
            <p class="note">通常営業の時間帯であっても、ご予約いただければ貸切は可能です。</p>
          </div>
          <div class="bx_dl">
            <dl>
              <dt>ご利用にあたって</dt>
              <dd>
                <ul>
                  <li>ご利用は30分単位、最短１時間から承ります。ご利用可能時間帯は10:30〜20:30までとなります（その他時間帯ご希望の場合はご相談ください）。<br>事前の内覧をご希望の方は問い合わせフォームよりその旨をお伝えください。</li>
                  <li>お手洗いはVisions LOUNGEを出たフロア内にございます。</li>
                  <li>スペース内は禁煙です。最寄りの喫煙所はビル内の別フロアにございます。</li>
                  <li>ケータリング等持ち込み可能です。ゴミの片付け等はご利用者様自身でおこなっていただくようお願いいたします。<br>※提携学生団体登録をおこなっていただいた団体は、スペースの優待利用が可能です（法人・社会人の登録は不可）。<br>詳細は「ご利用料金・ご利用規約」のページをご覧ください。</li>
                </ul>
              </dd>
            </dl>
            <dl>
              <dt>キャンセルについて</dt>
              <dd>
                <em>ご予約確定後のキャンセルについては下記のとおりキャンセル料が発生いたします。</em>
                <ul>
                  <li>・ご利用日４日〜前日まで：50%</li>
                  <li>・ご利用当日：100％</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!--/.bx_dl-->
          <div class="btn"><a href="/contact"><span>レンタルの予約をする</span></a></div>
        </div>
      </section>
      <!--/.st_cld-->
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer()?>
</body>
</html>
