window.addEventListener('DOMContentLoaded',function(){
  var equip = document.getElementById('equipment');
  equip.classList.add('active');
  equip.nextElementSibling.classList.add('active');
  // equip.addEventListener('click',function(e){
  //   if(this.classList.contains('active')){
  //     this.classList.remove('active');
  //     this.nextElementSibling.classList.remove('active');
  //   } else {
  //     this.classList.add('active');
  //     this.nextElementSibling.classList.add('active');
  //   }
  // })
  var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 8,
      slidesPerView: 10,
      loop: true,
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      navigation: {
        nextEl: '.swiper-control-next',
        prevEl: '.swiper-control-prev',
      },
      breakpoints: {
        769: {
          slidesPerView: 4,
        }
      }
    });
  var galleryTop = new Swiper('.gallery-top', {
      // spaceBetween: 10,
      autoHeight: true,
      loop:true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs
      },
      pagination: {
        el: '.swiper-pagi',
        type: 'fraction',
        formatFractionCurrent: function (number) {
          if(number < 10) {
            return '0'+number;
          } else {
            return number;
          }
        },
      },

    });
})