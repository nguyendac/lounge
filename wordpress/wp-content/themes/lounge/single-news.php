<?php
get_header();
?>
<body>
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div id="container" class="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main class="main">
        <section class="st_news">
          <div class="ttl">
            <div class="row">
              <picture class="effect maskToRight">
                <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/news/images/bkg_ttl_sp.png" />
                <img src="<?php bloginfo('template_url')?>/news/images/bkg_ttl_pc.jpg?v=ba6490aa1d80679349da958281646f31" alt="Price 01" />
              </picture>
              <h2>NEWS<span>ニュース</span></h2>
            </div>
          </div>
          <div class="row wrap">
            <div class="gr_news">
              <article>
                <?php 
                  $term = wp_get_post_terms(get_the_ID(),'cat_news',array("fields" => "all"));
                  $cat_name = 'No Cate';
                  if(count($term)) {
                    $cat_name = $term[0]->name;
                  }
                ?>
                <time datetime="<?php _e(get_the_date('Y-m-d'))?>"><?php _e(get_the_date('Y.m.d'))?></time>
                <h3>
                  <?php if(strtolower($cat_name) == 'news') :?>
                    <span class="green">NEWS</span>
                  <?php endif;?>
                  <?php if(strtolower($cat_name) == 'press') :?>
                    <span class="black">PRESS</span>
                  <?php endif;?>
                  <?php the_title()?></h3>
                <div class="main_art">
                  <?php _e(nl2br(the_content()))?>
                </div>
              </article>
              <!--/.bx_detail-->
              <div class="bx_pag pag_detail">
                <?php previous_post_link('%link','PREV'); ?>
                <a class="btn_hv" href="/news"><span>BACK TO LIST</span></a>
                <?php next_post_link('%link','NEXT'); ?>
              </div>
              <!--/.bx_pag-->
            </div>
            <!--/.gr_news-->
          </div>
          <!--/.wrap-->
        </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
  <?php get_footer();?>
  <?php endwhile; endif; ?>
</body>
</html>