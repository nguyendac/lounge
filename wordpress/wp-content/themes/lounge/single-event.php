<?php
get_header();
?>
<body>
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div id="container" class="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main class="main">
        <section class="event_detail">
          <figure class="banner_detail">
            <?php
            $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
            if(get_post_meta(get_the_ID(),'thumb',true)) {
              $img = get_post_meta(get_the_ID(),'thumb',true);
              $thumb = $img['url'];
            }
            ?>
            <img src="<?php _e($thumb)?>" alt="event banner" />
          </figure>
          <div class="event_ct">
            <div class="row">
              <div class="event_ct_inner">
                <div class="event_box_des">
                  <em class="event_box_cat event">EVENT</em>
                  <div class="event_box_link">
                    <time datetime="<?php _e(get_the_date('Y-m-d'))?>"><?php _e(get_the_date('Y.m.d'))?></time>
                    <?php
                      $terms = wp_get_post_terms($post->ID,'tag_event',array("fields" => "all"));
                    ?>
                    <?php foreach($terms as $term):?>
                    <span><?php _e($term->name)?></span>
                    <?php endforeach;?>
                  </div>
                </div>
                <?php _e(nl2br(the_content()))?>
                <!-- <div class="btn_green">
                  <a href="https://select-type.com/rsv/?id=I5IZCj85ZHE&c_id=45405&w_flg=1" target="_blank"><span>このイベントに予約する</span></a>
                </div> -->
              </div>
            </div>
          </div>
          <div class="other_post">
              <div class="row">
                <h3 class="other_post_tt">OTHER EVENTS</h3>
                <div class="other_post_inner">
                  <?php
                    $events = apply_filters('get_3_event','');
                    while ($events->have_posts()) : $events->the_post();
                    $terms = wp_get_post_terms($post->ID,'tag_event',array("fields" => "all"));
                  ?>
                  <div class="event_box">
                      <a href="<?php the_permalink()?>">
                        <div class="event_box_des">
                          <figure>
                            <?php
                              $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                              if(get_post_meta($post->ID,'thumb',true)) {
                                $img = get_post_meta($post->ID,'thumb',true);
                                $thumb = $img['url'];
                              }
                            ?>
                            <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                          </figure>
                          <div class="event_tag">
                              <span>
                                <sub><?php the_time('m')?></sub>
                                <em><?php the_time('d')?></em>
                              </span>
                              <small><?php echo substr(date('l',strtotime($date)),0,3) ?></small>
                          </div>
                          <p><?php the_title()?></p>
                        </div>
                        <div class="event_box_link">
                          <?php foreach($terms as $term):?>
                          <span><?php _e($term->name)?></span>
                          <?php endforeach;?>
                        </div>
                      </a>
                  </div>
                <?php endwhile;?>
                </div>
                <div class="bx_pag pag_detail">
                  <a class="btn_hv" href="/event"><span>BACK TO LIST</span></a>
                  <!--/.list_pag-->
                </div>
              </div>
          </div>
        </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
  <?php get_footer();?>
  <?php endwhile; endif; ?>
</body>
</html>