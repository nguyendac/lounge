<div class="bx_header">
  <div class="row wrap">
    <h1 class="bx_header_logo"><a href="<?php _e(home_url())?>"><img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="Logo"></a></h1>
    <div class="bx_header_r">
      <nav class="nav" id="nav">
        <a id="close_nav" href="" class="close show_sp">close</a>
        <ul>
          <li><a href="<?php _e(home_url())?>/about">アバウト</a></li>
          <li><a href="<?php _e(home_url())?>/visionsway">VisionsWay</a></li>
          <li><a href="<?php _e(home_url())?>/topics">トピックス</a></li>
          <li><a href="<?php _e(home_url())?>/spacerental">スペースレンタル</a></li>
          <li><a href="<?php _e(home_url())?>/contact">お問い合わせ</a></li>
        </ul>
        <ul class="show_sp">
          <li class="visionway"><a href="<?php _e(home_url())?>/visionsway">VisionsWay一期生募集中</a></li>
          <li class="login"><a href="https://select-type.com/member/login/?mi=LOXqAuRPuIY" target="_blank"><span>会員ログイン</span></a></li>
        </ul>
      </nav>
      <!--/.nav-->
      <ul class="btn_link">
        <li class="schedule"><a href="<?php _e(home_url())?>/event" data-content="SCHEDULE"><span>スケジュール</span></a></li>
        <li class="access"><a href="<?php _e(home_url())?>/access" data-content="ACCESS"><span>アクセス</span></a></li>
        <li class="login show_pc" id="login"><a href="https://select-type.com/member/login/?mi=LOXqAuRPuIY" target="_blank"><span>会員ログイン</span></a></li>
      </ul>
      <div class="bx_header_r_open show_sp" id="open_nav" data-content="MENU"><span>open</span></div>
    </div>
    <!--/.right-->
  </div>
  <!--/.wrap-->
</div>
<!--/.bx_header-->
