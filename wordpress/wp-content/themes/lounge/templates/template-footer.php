<div class="bx_footer">
  <div class="bx_footer_top">
    <div class="row wrap">
      <h2 class="logo_ft show_pc"><a href="<?php _e(home_url())?>"><img src="<?php bloginfo('template_url')?>/common/images/logo_ft.png" alt="Logo footer"></a></h2>
      <ul class="nav_ft">
        <li><a href="<?php _e(home_url())?>/about">アバウト</a></li>
        <li><a href="<?php _e(home_url())?>/news">ニュース</a></li>
        <li><a href="<?php _e(home_url())?>/partner">パートナー企業</a></li>
        <li><a href="<?php _e(home_url())?>/access">アクセス</a></li>
        <li><a href="<?php _e(home_url())?>/visionsway">Visions Way</a></li>
        <li><a href="<?php _e(home_url())?>/spacerental">スペースレンタル</a></li>
        <li><a href="<?php _e(home_url())?>/join">パートナーご希望の<br class="show_sp">企業様へ</a></li>
        <li><a href="<?php _e(home_url())?>/contact">お問い合わせ</a></li>
        <li><a href="<?php _e(home_url())?>/topics">トピックス</a></li>
        <li><a href="<?php _e(home_url())?>/price">利用料金・利用規約</a></li>
        <li><a href="<?php _e(home_url())?>/event">イベント&スケジュール</a></li>
        <li><a href="https://select-type.com/member/login/?mi=LOXqAuRPuIY" target="_blank">ログイン</a></li>
      </ul>
      <!--/.nav_ft-->
    </div>
    <!--/.wrap-->
  </div>
  <!--/.top-->
  <div class="bx_footer_bottom">
    <h2 class="logo_ft show_sp"><a href="<?php _e(home_url())?>"><img src="<?php bloginfo('template_url')?>/common/images/logo_ft.png" alt="Logo footer"></a></h2>
    <ul class="list_social">
      <li><a href="http://www.facebook.com/share.php?u=https://www.visions-lounge.com" target="_blank" rel="nofollow" onclick="ga( 'send', 'event', 'facebook', 'post-top', 'post-top', 1 );"><img src="<?php bloginfo('template_url')?>/common/images/icon_fb.png" alt="Icon Facebook"></a></li>
      <li><a href="https://twitter.com/share?url=https://www.visions-lounge.com&amp;text=Visions LOUNGE｜ビジョンズラウンジ大阪府" target="_blank" rel="nofollow" onclick="ga( 'send', 'event', 'twitter', 'post-top', 'post-top', 1 );">><img src="<?php bloginfo('template_url')?>/common/images/icon_tw.png" alt="Icon Tweeter"></a></li>
      <li><a href="http://b.hatena.ne.jp/add?mode=confirm&amp;url=https://www.visions-lounge.com&amp;title=オリジナルのシェアボタンを作ろう！各種SNSのボタン設置用URLまとめ" target="_blank" rel="nofollow" onclick="ga( 'send', 'event', 'hatena', 'post-top', 'post-top', 1 );"><img src="<?php bloginfo('template_url')?>/common/images/icon_b.png" alt="Icon B"></a></li>
      <li><a href="http://line.me/R/msg/text/?https://www.visions-lounge.com" target="_blank"><img src="<?php bloginfo('template_url')?>/common/images/icon_line.png" alt="Icon Line"></a></li>
    </ul>
    <!--/.list_social-->
    <p class="copyright">Copyright &copy;  Visions LOUNGE 2018</p>
  </div>
  <!--/.bottom-->
</div>
<!--/.bx_footer-->