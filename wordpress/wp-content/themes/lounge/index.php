<?php get_header();?>
<body>
<div id="container" class="container">
  <header id="header" class="header">
    <?php get_template_part('templates/template','header')?>
  </header>
  <main class="main">
    <div id="slider" class="slider">
      <div class="slider_under" id="under">
        <ul>
          <li>
            <picture>
              <source media="(max-width:768px)" srcset="<?php bloginfo('template_url')?>/images/slide_01_sp.png">
              <img src="<?php bloginfo('template_url')?>/images/slide_01.jpg" alt="slide 01">
            </picture>
          </li>
          <li>
            <picture>
              <source media="(max-width:768px)" srcset="<?php bloginfo('template_url')?>/images/slide_02_sp.png">
              <img src="<?php bloginfo('template_url')?>/images/slide_02.jpg" alt="slide 02">
            </picture>
          </li>
          <li>
            <picture>
              <source media="(max-width:768px)" srcset="<?php bloginfo('template_url')?>/images/slide_03_sp.png">
              <img src="<?php bloginfo('template_url')?>/images/slide_03.jpg" alt="slide 03">
            </picture>
          </li>
          <li>
            <picture>
              <source media="(max-width:768px)" srcset="<?php bloginfo('template_url')?>/images/slide_04_sp.png">
              <img src="<?php bloginfo('template_url')?>/images/slide_04.jpg" alt="slide 04">
            </picture>
          </li>
        </ul>
      </div>
      <div class="slider_front" id="front">
        <figure>
          <span><img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt="vision lounge"></span>
          <figcaption>
            <span>生きること、働くことを</span><br class="show_pc"><span>本気で楽しみたい学生や若手社会人のための場所、Visions LOUNGE。</span><br><span>かっこよく生きる人たちの様々な人生や価値観にふれ、</span><br><span>自分らしさが明確になる。</span><br><span>自分だからできることに気づく。成し遂げたい未来が見えてくる。</span><br><span>一人ひとりのVisionが育っていくきっかけが、</span><br><span>ここにはあります。</span>
          </figcaption>
        </figure>
      </div>
      <div class="slider_flag">
        <a href="<?php _e(home_url())?>/visionsway"><img src="<?php bloginfo('template_url')?>/images/flag@2x.png" alt="flag"></a>
      </div>
      <div class="social show_pc">
        <ul>
          <li><a href="https://www.facebook.com/visionslounge.umeda/" target="_blank" class="icon_face">facebook</a></li>
          <li><a href="https://twitter.com/visions_lounge" target="_blank" class="icon_twitter">twitter</a></li>
          <li><a href="https://line.me/R/ti/p/%40fcc3569i" target="_blank" class="icon_line">line</a></li>
        </ul>
      </div>
    </div><!-- end slider  -->
    <section id="news" class="news row">
      <div class="news_main">
        <h2>News</h2>
        <ul>
          <?php
          $news = apply_filters('get_2_news','');
          if($news->post_count > 0) :
            while ($news->have_posts()) : $news->the_post();
          ?>
          <li><a href="<?php the_permalink()?>">
              <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
              <span><?php the_title()?></span>
            </a></li>
          <?php endwhile;wp_reset_query();endif;?>
        </ul>
        <a href="/news">more</a>
      </div>
    </section>
    <section id="member" class="member">
      <figure class="maskToRight effect show_pc"><img src="<?php bloginfo('template_url')?>/images/vw_img@2x.jpg" alt="vision way"></figure>
      <div class="member_pos">
        <div class="member_m row">
          <div class="member_m_w full fadeInUp">
            <h2>MEMBERSHIP<br>PROGRAM</h2>
            <figure><img src="<?php bloginfo('template_url')?>/images/visionway@2x.png" alt="vision"></figure>
            <figure class="maskToRight effect show_sp"><img src="<?php bloginfo('template_url')?>/images/vw_img@2x.jpg" alt="vision way"></figure>
            <p>自分らしい幸せな未来像を見つけるための、メンバーシップ制オリジナルプログラム“VisionsWay”。志を胸に時代を切り拓く経営者やエース社員たちの熱い想いに｢直｣に触れ、語り合う中で、自らの価値観を研ぎ澄ませていく体験が、ここに。　</p>
            <div class="btn bx_pag ">
              <a class="btn_hv" href="/visionsway"><span>MORE</span></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="event"  class="event">
      <div class="event_main row">
        <h2>UP COMING EVENT</h2>
        <div class="event_list">
          <div class="event_wrap">
            <div class="swiper-container">
              <div class="event_list_box slick swiper-wrapper">
                <?php
                  $topics = apply_filters('get_6_event','');
                  if($topics->post_count > 0) :
                ?>
                <?php
                  while ($topics->have_posts()) : $topics->the_post();
                  $terms = wp_get_post_terms($post->ID,'tag_event',array("fields" => "all"));?>
                <div class="event_box swiper-slide">
                  <a href="<?php the_permalink();?>">
                    <div class="event_box_des">
                      <figure class="effect maskToRight">
                        <?php
                        $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                        if(get_post_meta($post->ID,'thumb',true)) {
                          $img = get_post_meta($post->ID,'thumb',true);
                          $thumb = $img['url'];
                        }
                        ?>
                        <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                      </figure>
                      <?php if(get_post_meta($post->ID,'date_event',true)):?>
                      <div class="event_tag">
                        <span>
                          <?php 
                            $date = get_post_meta($post->ID,'date_event',true);
                          ?>
                          <sub><?php echo date('m',strtotime($date)) ?></sub>
                          <em><?php echo date('d',strtotime($date)) ?></em>
                        </span>
                        <small><?php echo substr(date('l',strtotime($date)),0,3) ?></small>
                      </div>
                    <?php endif;?>
                      <p><?php the_title()?></p>
                    </div>
                    <div class="event_box_link">
                      <?php foreach($terms as $term):?>
                      <span><?php _e($term->name)?></span>
                      <?php endforeach;?>
                    </div>
                  </a>
                </div>
                <div class="event_box swiper-slide">
                  <a href="<?php the_permalink();?>">
                    <div class="event_box_des">
                      <figure class="effect maskToRight">
                        <?php
                        $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                        if(get_post_meta($post->ID,'thumb',true)) {
                          $img = get_post_meta($post->ID,'thumb',true);
                          $thumb = $img['url'];
                        }
                        ?>
                        <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                      </figure>
                      <?php if(get_post_meta($post->ID,'date_event',true)):?>
                      <div class="event_tag">
                        <span>
                          <?php 
                            $date = get_post_meta($post->ID,'date_event',true);
                          ?>
                          <sub><?php echo date('m',strtotime($date)) ?></sub>
                          <em><?php echo date('d',strtotime($date)) ?></em>
                        </span>
                        <small><?php echo substr(date('l',strtotime($date)),0,3) ?></small>
                      </div>
                    <?php endif;?>
                      <p><?php the_title()?></p>
                    </div>
                    <div class="event_box_link">
                      <?php foreach($terms as $term):?>
                      <span><?php _e($term->name)?></span>
                      <?php endforeach;?>
                    </div>
                  </a>
                </div>
                <div class="event_box swiper-slide">
                  <a href="<?php the_permalink();?>">
                    <div class="event_box_des">
                      <figure class="effect maskToRight">
                        <?php
                        $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                        if(get_post_meta($post->ID,'thumb',true)) {
                          $img = get_post_meta($post->ID,'thumb',true);
                          $thumb = $img['url'];
                        }
                        ?>
                        <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                      </figure>
                      <?php if(get_post_meta($post->ID,'date_event',true)):?>
                      <div class="event_tag">
                        <span>
                          <?php 
                            $date = get_post_meta($post->ID,'date_event',true);
                          ?>
                          <sub><?php echo date('m',strtotime($date)) ?></sub>
                          <em><?php echo date('d',strtotime($date)) ?></em>
                        </span>
                        <small><?php echo substr(date('l',strtotime($date)),0,3) ?></small>
                      </div>
                    <?php endif;?>
                      <p><?php the_title()?></p>
                    </div>
                    <div class="event_box_link">
                      <?php foreach($terms as $term):?>
                      <span><?php _e($term->name)?></span>
                      <?php endforeach;?>
                    </div>
                  </a>
                </div>
                <div class="event_box swiper-slide">
                  <a href="<?php the_permalink();?>">
                    <div class="event_box_des">
                      <figure class="effect maskToRight">
                        <?php
                        $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                        if(get_post_meta($post->ID,'thumb',true)) {
                          $img = get_post_meta($post->ID,'thumb',true);
                          $thumb = $img['url'];
                        }
                        ?>
                        <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                      </figure>
                      <?php if(get_post_meta($post->ID,'date_event',true)):?>
                      <div class="event_tag">
                        <span>
                          <?php 
                            $date = get_post_meta($post->ID,'date_event',true);
                          ?>
                          <sub><?php echo date('m',strtotime($date)) ?></sub>
                          <em><?php echo date('d',strtotime($date)) ?></em>
                        </span>
                        <small><?php echo substr(date('l',strtotime($date)),0,3) ?></small>
                      </div>
                    <?php endif;?>
                      <p><?php the_title()?></p>
                    </div>
                    <div class="event_box_link">
                      <?php foreach($terms as $term):?>
                      <span><?php _e($term->name)?></span>
                      <?php endforeach;?>
                    </div>
                  </a>
                </div>
              <?php endwhile;wp_reset_query();endif;?>
              </div>
              <div class="swiper-scrollbar show_sp"></div>
            </div>
            <div class="swiper-next swiper-button"></div>
            <div class="swiper-prev swiper-button"></div>
          </div>
          <div class="btn bx_pag ">
            <a class="btn_hv" href="/event"><span>MORE</span></a>
          </div>
        </div>
      </div>
    </section>
    <section id="schedule" class="schedule row">
      <div class="schedule_left">
        <h2>SCHEDULE</h2>
        <!-- <ul>
          <li><a href="#">
              <time datetime="2018-09-10">2018.09.10</time>
              <span>【申込み締切】【連続講座】TOKYO-DOCAN 第5期（全7回 10/7〜11/25）</span>
            </a></li>
          <li><a href="#">
              <time datetime="2018-09-10">2018.09.10</time>
              <span>休館日</span>
            </a></li>
          <li><a href="#">
              <time datetime="2018-09-10">2018.09.10</time>
              <span>就活生交流イベント</span>
            </a></li>
          <li><a href="#">
              <time datetime="2018-09-10">2018.09.10</time>
              <span>初心者大歓迎！無料で最先端のプログラミングを学ぼう！</span>
            </a></li>
        </ul>
        <p><span class="yellow">イベント予定</span><span class="blue">貸切予定</span><span class="red">休館日</span></p> -->
      </div>
      <div class="schedule_right">
        <?php //if (is_active_sidebar('calendar')):?>
          <?php //dynamic_sidebar('calendar'); ?>
        <?php //endif; ?>
        <?php
          $wrap_calendar = get_bloginfo('template_url').'/gcalendar-wrapper.php';
        ?>
        <iframe src="<?php _e($wrap_calendar) ?>?title=Visions%20LOUNGE&amp;showTitle=0&amp;showDate=0&amp;showPrint=0&amp;showCalendars=0&amp;showTz=0&amp;mode=WEEK&amp;height=600&amp;wkst=2&amp;hl=ja&amp;bgcolor=%23FFFFFF&amp;src=8lm9iqij1u4109tc7lhk2qloac%40group.calendar.google.com&amp;color=%232F6309&amp;src=gmqsiv0ab5fldl3nfis6untu7s%40group.calendar.google.com&amp;color=%23AB8B00&amp;ctz=Asia%2FTokyo" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
        <p><span class="blue">ラウンジスペースの予定（営業時間・イベント・貸切）</span><span class="yellow">会議室の予定</span></p>
      </div>
    </section>
    <section id="topics" class="topics">
      <div class="topics_main row">
        <h2>TOPICS</h2>
        <div class="topics_list_box">
          <?php
            $topics = apply_filters('get_4_topics','');
            while ($topics->have_posts()) : $topics->the_post();
            $term = wp_get_post_terms($post->ID,'cat_topic',array("fields" => "all"));
            $cat_name = 'No Cate';
            if(count($term)) {
              $cat_name = $term[0]->name;
            }
            if(strtolower($cat_name) === 'media') {
              $target = true;
            } else {
              $target = false;
            }
          ?>
          <div class="topics_box">
              <?php if($target) : ?>
                <a href="<?php _e(get_post_meta($post->ID,'link',true))?>" target="_blank">
              <?php else:?>
              <a href="<?php the_permalink()?>">
              <?php endif;?>
              <figure>
                <span class="effect maskToRight">
                  <?php
                    $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                    if(get_post_meta($post->ID,'thumb',true)) {
                      $img = get_post_meta($post->ID,'thumb',true);
                      $thumb = $img['url'];
                    }
                  ?>
                  <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                </span>
                <?php if($target):?>
                  <figcaption class="topics_box_cat media"><?php _e($cat_name)?></figcaption>
                <?php else:?>
                  <figcaption class="topics_box_cat blog"><?php _e($cat_name)?></figcaption>
                <?php endif;?>
              </figure>
              <div class="topics_box_des">
                <p><?php the_title()?></p>
                <div class="topics_box_link">
                  <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
                  <?php if($target):?>
                    <span>外部リンクへ</span>
                  <?php endif;?>
                </div>
              </div>
            </a>
          </div>
        <?php endwhile;wp_reset_query();?>
        </div>
        <div class="btn bx_pag ">
          <a class="btn_hv" href="/topics"><span>MORE</span></a>
        </div>
      </div>
    </section>
    <section id="partner" class="partner">
      <div class="partner_top">
        <figure class="effect maskToRight"><img src="<?php bloginfo('template_url')?>/images/partner_img.jpg" alt=""></figure>
        <div class="partner_pos">
          <div class="partner_m row">
            <div class="partner_m_w effect fadeInUp">
              <h2>PARTNER <br>COMPANY</h2>
              <p>Visions LOUNGEの考え方に共感しご協力いただいているパートナー企業をご紹介します。</p>
              <div class="partner_btn">
                <a href="/partner"><span>パートナーおよびラウンジ学生クルー希望の皆様へ</span></a>
              </div>
              <div class="btn bx_pag ">
                <a class="btn_hv" href="/join"><span>MORE</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <div id="rental_price" class="rp row">
      <div class="rp_box">
        <figure>
          <a href="/spacerental" class="effect maskToRight"><img src="<?php bloginfo('template_url')?>/images/space_img.jpg" alt="space"></a>
          <figcaption>SPACE RENTAL</figcaption>
        </figure>
        <p>Visions LOUNGEは梅田駅徒歩０分。<br>企業向け、学生団体向けのスペースレンタルもおこなっています。</p>
        <div class="btn bx_pag ">
          <a class="btn_hv" href="/spacerental"><span>MORE</span></a>
        </div>
      </div>
      <div class="rp_box">
        <figure>
          <a href="/price" class="effect maskToRight"><img src="<?php bloginfo('template_url')?>/images/price_img.jpg" alt="price"></a>
          <figcaption>PRICE</figcaption>
        </figure>
        <p>Visions LOUNGEをご利用いただくにあたっての<br>各種料金・規約をご覧いただけます。</p>
        <div class="btn bx_pag ">
          <a class="btn_hv" href="/price"><span>MORE</span></a>
        </div>
      </div>
    </div>
    <section id="access" class="st_access">
      <div id="map" class="st_access_map"></div>
      <div class="st_access_content">
        <h2>ACCESS</h2>
        <h3>阪急梅田駅直結、徒歩0分。</h3>
        <h4>Visions LOUNGE Umeda</h4>
        <address>〒530-0012<br>大阪府大阪市北区芝田1-1-4 阪急ターミナルビル7F</address>
        <a class="tel" href="tel:06-6377-5646">TEL : 06-6377-5646</a>
        <div class="btn bx_pag ">
          <a class="btn_hv" href="/access"><span>MORE</span></a>
        </div>
      </div>
    </section>
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/template','footer')?>
  </footer>
</div>
<?php get_footer();?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3sl4xld55qkjnuWOIzUkpFgBB2caWsxI&callback=initMap"></script>
</body>
</html>