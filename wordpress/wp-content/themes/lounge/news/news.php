<?php
/*
  Template Name: News Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_news">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/news/images/bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/news/images/bkg_ttl_pc.jpg?v=ba6490aa1d80679349da958281646f31" alt="Price 01" />
            </picture>
            <h2>NEWS<span>ニュース</span></h2>
          </div>
        </div>
        <div class="row wrap">
          <div class="gr_news">
            <ul class="gr_news_list">
              <?php 
              $news = apply_filters('list_news','',$_GET);
              while ($news->have_posts()) : $news->the_post();
              $term = wp_get_post_terms($post->ID,'cat_news',array("fields" => "all"));
              $cat_name = 'No Cate';
              if(count($term)) {
                $cat_name = $term[0]->name;
              }
              ?>
              <li>
                <a href="<?php the_permalink()?>">
                  <time datetime="<?php the_time('Y.m.d')?>"><?php the_time('Y-m-d')?></time>
                  <p>
                    <?php if(strtolower($cat_name) == 'news') :?>
                      <span class="green">NEWS</span>
                    <?php endif;?>
                    <?php if(strtolower($cat_name) == 'press') :?>
                      <span class="black">PRESS</span>
                    <?php endif;?>
                    <em><?php the_title()?></em>
                  </p>
                </a>
              </li>
              <?php endwhile;wp_reset_query();?>
            </ul>
            <!--/.list-->
            <div class="bx_pag">
              <div class="bx_pag_l">
                <?php
                mp_pagination($prev = 'PREV', $next = 'NEXT', $pages=$news->max_num_pages);
                wp_reset_query();
                ?>
              </div>
              <!--/.list_pag-->
            </div>
            <!--/.bx_pag-->
          </div>
          <!--/.gr_news-->
        </div>
        <!--/.wrap-->
      </section>
      <!--/.st_news-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer()?>

</body>
</html>