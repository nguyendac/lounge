<?php
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="topics">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/topics/images/topics_banner_sp.png" />
              <img src="<?php bloginfo('template_url')?>/topics/images/topics_banner.png?v=b5b284eded3efcfd0e78bb280f94c0a8" alt="Topics banner" />
            </picture>
            <h2>TOPICS<span>トピックス</span></h2>
          </div>
        </div>
        <div class="topics_ct">
          <div class="topics_ct_des">
            <div class="row">
              <p>Visions LOUNGEからのお知らせやイベントレポートをご覧いただけます。<br>また、MEDIAカテゴリーでは、独自のVisionを掲げる企業を特集しその背景にあるストーリーをご紹介しています。</p>
            </div>
          </div>
          <div class="topics_list">
            <div class="row">
              <div class="topics_list_cat">
                <h3>CATEGORY</h3>
                <ul id="according" class="according">
                  <li data-type="0"><a href="/topics">ALL</a></li>
                  <?php
                  $term_name = get_term_by('slug',get_query_var('term'),get_query_var( 'taxonomy' ));
                  $types = apply_filters('list_taxo','cat_topic');
                  if($types):
                  foreach($types as $key => $type) :
                  ?>
                  <?php if($type->name == $term_name->name):?>
                    <li data-type="<?php _e($key+1)?>" class="active"><a class="active" href="javascript:void(0)"><?php _e(nl2br($type->name)) ?></a>
                  <?php else:?>
                    <li data-type="<?php _e($key+1)?>"><a href="<?php _e(get_term_link($type))?>"><?php _e(nl2br($type->name)) ?></a></li>
                  <?php endif;?>
                <?php endforeach;endif;?>
                </ul>
              </div>
              <div class="topics_list_box">
                <?php 
                $topics = apply_filters('list_topics',$term_name,$_GET);
                while ($topics->have_posts()) : $topics->the_post();
                $cat = wp_get_post_terms($post->ID,'cat_topic',array("fields" => "all"));
                $cat = $cat[0];
                if(count($cat)) {
                  $cat_name = $cat->name;
                }
                if(strtolower($cat_name) === 'media') {
                  $target = true;
                } else {
                  $target = false;
                }
                ?>
                <div class="topics_box effect fadeInUp">
                    <?php if($target) : ?>
                      <a href="<?php _e(get_post_meta($post->ID,'link',true))?>" target="_blank">
                    <?php else:?>
                      <a href="<?php the_permalink()?>">
                    <?php endif;?>
                    <figure>
                      <?php 
                      $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                      if(get_post_meta($post->ID,'thumb',true)) {
                        $img = get_post_meta($post->ID,'thumb',true);
                        $thumb = $img['url'];
                      }
                      ?>
                      <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                    </figure>
                    <div class="topics_box_des">
                      <?php if($target):?>
                        <em class="topics_box_cat media">MEDIA</em>
                      <?php else:?>
                        <em class="topics_box_cat blog"><?php _e($cat_name)?></em>
                      <?php endif;?>
                      <p><?php the_title()?></p>
                      <div class="topics_box_link">
                        <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
                        <?php if($target):?>
                          <span>外部リンクへ</span>
                        <?php endif;?>
                      </div>
                    </div>
                  </a>
                </div>
                <?php endwhile;wp_reset_query();?>
              </div>
              <div class="bx_pag">
                <div class="bx_pag_l">
                  <?php
                  mp_pagination($prev = 'PREV', $next = 'NEXT', $pages=$topics->max_num_pages);
                  wp_reset_query();
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>

</body>
</html>
