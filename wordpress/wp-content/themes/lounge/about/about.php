<?php
/*
  Template Name: About Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_about">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/about/images//bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/about/images/bkg_ttl_pc.jpg" alt="Price 01" />
            </picture>
            <h2>ABOUT<span>ラウンジについて</span></h2>
          </div>
        </div>
        <!--/.ttl-->
        <div class="gr_about">
          <div class="gr_about_top">
            <div class="top_pos">
              <div class="top_pos_m row">
                <div class="top_pos_m_w">
                  <h3>ここは、Visionが育つ場所。</h3>
                  <p>Visions LOUNGEの”Visions”とは、<br class="show_pc">
                  <span>「志を持って生きていこうとする人たち」</span>を意味します。<br><br class="show_sp">
                  自分らしい志を見つけ、実現しようと日々本気で生きている大人たち。<br class="show_pc">
                  まだはっきりとはしない志の片鱗に向き合いながら、<br class="show_pc">
                  未来を描こうとしている若者たち。</p>
                  <p>そんな人々がお互いの価値観や発想を共有し、<br class="show_pc">
                  刺激しあえる場所があればもっと面白い世の中になるんじゃないか。<br class="show_pc">
                  そんな想いでこの場所は生まれました。</p>
                </div>
              </div>
            </div>
            <figure><span class="effect maskToRight"><img src="<?php bloginfo('template_url')?>/about/images/img_01.jpg?v=5f360c357e64e8989967ad1705d379a0" alt="Images 01"></span></figure>
          </div>
          <div class="gr_about_bottom">
            <div class="row wrap">
              <figure><span class="effect maskToRight"><img src="<?php bloginfo('template_url')?>/about/images/img_02.jpg?v=52dc318e8f336dc391dbe6708dff5c1e" alt="Images 02"></span></figure>
              <div class="bx_txt">
                <p>「自分はなんのために生まれてきたのか」<br class="show_pc">
                  「この人生を本当に幸せに生きるとはどういうことか」<br class="show_pc">
                  考えてみたことはありますか？</p>
                  <p><span>志をもって人や社会に役立っていくことで、<br class="show_pc">
                  心から自分を応援し愛してくれる人たちに囲まれて生きること。</span><br class="show_pc">
                  それが、一人ひとりの存在意義であり、幸せな人生だと私たちは思います。</p>
                  <p>志は、すぐには見つからないかもしれません。<br>だからこそ、Visions LOUNGEは一人でも多くの人が自分の志を見つけ、幸せな人生を歩んでいくためのきっかけをつくり続けます。</p>
              </div>
            </div>
          </div>
        </div>
        <!--/.gr_about-->
      </section>
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>

</body>
</html>
