<?php
require_once("meta-box-class/my-meta-box-class.php");
require_once("Tax-meta-class/tax-meta-class.php");
function enqueue_script(){
  wp_enqueue_script('libs',get_template_directory_uri().'/common/js/libs.js', '1.0', 1 );
  if(is_front_page()) {
    wp_enqueue_script('toppage', get_template_directory_uri().'/js/script.js', '1.0', 1 );
    wp_enqueue_script('swiper', get_template_directory_uri().'/js/swiper.js', '1.0', 1 );
  }
  if(is_page_template('access/access.php')) {
    wp_enqueue_script('access', get_template_directory_uri().'/access/js/scripts.js', '1.0', 1 );
  }
  if(is_page_template('event/event.php')) {
    // wp_enqueue_script('event', get_template_directory_uri().'/event/js/scripts.js', '1.0', 1 );
  }
  if(is_page_template('topics/topics.php') || is_tax('cat_topic')) {
    wp_enqueue_script('topics', get_template_directory_uri().'/topics/js/scripts.js', '1.0', 1 );
  }
  if(is_page_template('rental/spacerental.php')) {
    wp_enqueue_script('swiper', get_template_directory_uri().'/js/swiper.js', '1.0', 1 );
    wp_enqueue_script('rental', get_template_directory_uri().'/rental/js/script.js', '1.0', 1 );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_script');
function enqueue_style() {
  if(is_front_page()) {
    wp_enqueue_style('swiper',get_template_directory_uri().'/css/swiper.css');
    wp_enqueue_style('toppage',get_stylesheet_directory_uri().'/css/style.css');
  }
  if(is_page_template('about/about.php')) {
    wp_enqueue_style( 'about-style', get_stylesheet_directory_uri() . '/about/css/style.css');
  }
  if(is_page_template('access/access.php')) {
    wp_enqueue_style( 'access-style', get_stylesheet_directory_uri() . '/access/css/style.css');
  }
  if(is_page_template('contact/contact.php') || is_page_template('contact/contact-confirm.php') || is_page_template('contact/contact-thanks.php')) {
    wp_enqueue_style( 'contact-style', get_stylesheet_directory_uri() . '/contact/css/style.css');
  }
  if(is_page_template('price/price.php')) {
    wp_enqueue_style( 'price-style', get_stylesheet_directory_uri() . '/price/css/style.css');
  }
  if(is_page_template('visionsway/visionsway.php')) {
    wp_enqueue_style( 'visionsway-style', get_stylesheet_directory_uri() . '/visionsway/css/style.css');
  }
  if(is_page_template('rental/spacerental.php')) {
    wp_enqueue_style('swiper',get_template_directory_uri().'/css/swiper.css');
    wp_enqueue_style( 'spacerental-style', get_stylesheet_directory_uri() . '/rental/css/style.css');
  }
  if(is_page_template('join/join.php')) {
    wp_enqueue_style( 'join-style', get_stylesheet_directory_uri() . '/join/css/style.css');
  }
  if(is_page_template('partner/partner.php')) {
    wp_enqueue_style( 'partner-style', get_stylesheet_directory_uri() . '/partner/css/style.css');
  }
  if(is_singular('news') || is_page_template('news/news.php')) {
    wp_enqueue_style( 'news-style', get_stylesheet_directory_uri() . '/news/css/style.css');
  }
  if(is_singular('event') || is_page_template('event/event.php')) {
    wp_enqueue_style( 'event-style', get_stylesheet_directory_uri() . '/event/css/style.css');
  }
  if(is_singular('topics') || is_page_template('topics/topics.php') || is_tax('cat_topic')) {
    wp_enqueue_style( 'topics-style', get_stylesheet_directory_uri() . '/topics/css/style.css');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_style' );
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

// post type news //
function news_arr(){
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'List News',
      'singular_name' => 'news',
      'add_new_item' => 'New News',
    ),
    'supports' => array(
      'title',
      'editor',
      'excerpt'
    )
  );
  register_post_type('news',$args);
}
add_action('init','news_arr');
function create_type_news(){
  $labels = array(
    'name'              => _x( 'Category News', 'taxonomy general name' ),
    'singular_name'     => _x( 'By Type Category News', 'taxonomy singular name' ),
    'search_items'      => __( 'Search By Category News' ),
    'all_items'         => __( 'All By Category News' ),
    'parent_item'       => __( 'Parent By Category News' ),
    'parent_item_colon' => __( 'Parent By Category News:' ),
    'edit_item'         => __( 'Edit By Category News' ),
    'update_item'       => __( 'Update By Category News' ),
    'add_new_item'      => __( 'Add New By Category News' ),
    'new_item_name'     => __( 'New By Category News' ),
    'menu_name'         => __( 'By Category News' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'parent_item'       => null,
    'parent_item_colon' => null,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'cat-news' ),
  );
  register_taxonomy( 'cat_news', array( 'news' ), $args );
}
add_action( 'init', 'create_type_news', 0 );
// end post type news //

// post type topics //
function topic_arr(){
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'List Topics',
      'singular_name' => 'topics',
      'add_new_item' => 'New Topics',
    ),
    'supports' => array(
      'title',
      'editor',
      'excerpt'
    )
  );
  register_post_type('topics',$args);
}
add_action('init','topic_arr');
$config_topic_ex = array(
  'id' => 'topics_link',
  'title' => 'Topics Media Link',
  'pages' => array('topics'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$topic_ex_meta = new AT_Meta_Box($config_topic_ex);
$topic_ex_meta->addText('link',array('name'=> 'Blank','style'=>'max-width :100%; width:100%; height:auto'));
$topic_ex_meta->Finish();
function event_arr(){
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'List Event',
      'singular_name' => 'Event',
      'add_new_item' => 'New Event',
    ),
    'supports' => array(
      'title',
      'editor',
      'excerpt'
    )
  );
  register_post_type('event',$args);
}
add_action('init','event_arr');
$config_event = array(
  'id' => 'events',
  'title' => 'Date Event',
  'pages' => array('event'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$event_meta =  new AT_Meta_Box($config_event);
$event_meta->addDate('date_event',array('name'=> 'Date Event','format'=>'dd-mm-yy'));
$event_meta->Finish();
$config_topic = array(
  'id' => 'topics',
  'title' => 'Topics',
  'pages' => array('topics','event'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$topic_meta = new AT_Meta_Box($config_topic);
$topic_meta->addImage('thumb',array('name'=> 'Thumbnail','style'=>'max-width :100%; width:100%; height:auto'));
$topic_meta->Finish();
function partner_arr() {
  $args = array(
    'public'=> true,
    'labels' => array(
      'name' => 'List Partner',
      'singular_name' => 'Partner',
      'add_new_item' => 'New Partner',
    ),
    'supports' => array(
      'title',
    )
  );
  register_post_type('partner',$args);
}
add_action('init','partner_arr');
$config_partner = array(
  'id' => 'partner',
  'title' => 'Partner',
  'pages' => array('partner'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$partner_meta = new AT_Meta_Box($config_partner);
$partner_meta->addImage('logo',array('name'=> 'Logo Partner','style'=>'max-width :100%; width:100%; height:auto'));
$partner_meta->addText('link',array('name'=> 'Website'));
$partner_meta->addTextarea('desc',array('name'=> 'Description'));
$partner_meta->Finish();
function create_type_topic(){
  $labels = array(
    'name'              => _x( 'Category Topics', 'taxonomy general name' ),
    'singular_name'     => _x( 'By Type Category Topics', 'taxonomy singular name' ),
    'search_items'      => __( 'Search By Category Topics' ),
    'all_items'         => __( 'All By Category Topics' ),
    'parent_item'       => __( 'Parent By Category Topics' ),
    'parent_item_colon' => __( 'Parent By Category Topics:' ),
    'edit_item'         => __( 'Edit By Category Topics' ),
    'update_item'       => __( 'Update By Category Topics' ),
    'add_new_item'      => __( 'Add New By Category Topics' ),
    'new_item_name'     => __( 'New By Category Topics' ),
    'menu_name'         => __( 'By Category Topics' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'parent_item'       => null,
    'parent_item_colon' => null,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'cat-topic' ),
  );
  register_taxonomy( 'cat_topic', array( 'topics' ), $args );
}
add_action( 'init', 'create_type_topic', 0 );
// end topics //

// event //
function create_type_event(){
  $labels = array(
    'name'              => _x( 'Category Event', 'taxonomy general name' ),
    'singular_name'     => _x( 'By Type Category Event', 'taxonomy singular name' ),
    'search_items'      => __( 'Search By Category Event' ),
    'all_items'         => __( 'All By Category Event' ),
    'parent_item'       => __( 'Parent By Category Topics' ),
    'parent_item_colon' => __( 'Parent By Category Event:' ),
    'edit_item'         => __( 'Edit By Category Event' ),
    'update_item'       => __( 'Update By Category Event' ),
    'add_new_item'      => __( 'Add New By Category Event' ),
    'new_item_name'     => __( 'New By Category Event' ),
    'menu_name'         => __( 'By Category Event' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'parent_item'       => null,
    'parent_item_colon' => null,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'cat-event' ),
  );
  register_taxonomy( 'cat_event', array( 'event' ), $args );
}
add_action( 'init', 'create_type_event', 0 );
function create_tag_event(){
  $labels = array(
    'name'              => _x( 'Name Tag Event', 'taxonomy general tag name' ),
    'singular_name'     => _x( 'Tag Event', 'taxonomy singular tag name' ),
    'search_items'      => __( 'SearchTag  Event' ),
    'all_items'         => __( 'All Tag Event' ),
    'parent_item'       => null,
    'parent_item_colon' => null,
    'edit_item'         => __( 'Edit Tag Event' ),
    'update_item'       => __( 'Update Tag Event' ),
    'add_new_item'      => __( 'Add Tag New Event' ),
    'new_item_name'     => __( 'New Tag Event' ),
    'menu_name'         => __( 'Tag Event' ),
  );
  $args = array(
    'hierarchical' => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'tag_event' ),
  );
  register_taxonomy( 'tag_event', array( 'event' ), $args );
}
add_action( 'init', 'create_tag_event', 0 );
// end event //

function mp_pagination($prev = 'Prev', $next = 'Next', $pages='') {
    global $wp_query, $wp_rewrite, $textdomain;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    if(array_key_exists('pg',$_GET)){
      if($_GET['pg']) {
        $pg = $_GET['pg'];
      } else {
        $pg = 1;
      }
    } else {
      $pg = 1;
    }
    $big = 999999999;
    $pagination = array(
    'base'      => @add_query_arg( 'pg', '%#%' ),
    'format'   => '',
    'current'   => max ( 1, $pg),
    'total'   => $pages,
    'prev_text' => __($prev,$textdomain),
    'next_text' => __($next,$textdomain),
    'type'   => 'list',
    'end_size'  => 1,
    'mid_size'  => 2
    );
    $return =  paginate_links( $pagination );
    echo str_replace( '<ul class="page-numbers">', '<ul class="page-nums">', $return );
    //echo $return;
}
add_filter('next_posts_link_attributes', 'posts_link_next_attributes');
function add_class_next_post_link($html){
  $html = str_replace('<a','<a class="next"',$html);
  return $html;
}
add_filter('next_post_link','add_class_next_post_link',10,1);
function add_class_previous_post_link($html){
  $html = str_replace('<a','<a class="prev"',$html);
  return $html;
}
add_filter('previous_post_link','add_class_previous_post_link',10,1);
function action_2_news(){
  $args = array(
    'post_type' => 'news',
    'orderby'   => 'id',
    'order'     => 'desc',
    'posts_per_page' => 2
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_2_news','action_2_news');
function action_6_event(){
  $args = array(
    'post_type' => 'event',
    'orderby'   => 'id',
    'order'     => 'desc',
    'posts_per_page' => 6
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_6_event','action_6_event');
function action_4_topics(){
  $args = array(
    'post_type' => 'topics',
    'orderby'   => 'id',
    'order'     => 'desc',
    'posts_per_page' => 4
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_4_topics','action_4_topics');
function action_3_topics(){
  global $post;
  $args = array(
    'post_type' => 'topics',
    'orderby'   => 'id',
    'order'     => 'desc',
    'post__not_in' => array($post->ID),
    'posts_per_page' => 3
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_3_topics','action_3_topics');
function action_3_event(){
  global $post;
  $args = array(
    'post_type' => 'event',
    'orderby'   => 'id',
    'order'     => 'desc',
    'post__not_in' => array($post->ID),
    'posts_per_page' => 3
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_3_event','action_3_event');
function action_news($name = '',$get){
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $search_tax = array();
  if($name) {
    $tax = array(
      'taxonomy' => 'cat_news',
      'field' => 'slug',
      'terms' => $name
    );
    array_push($search_tax,$tax);
  }
  $args = array(
    'post_type' => 'news',
    'orderby'   => 'id',
    'order'     => 'desc',
    'paged' => $paged,
    'tax_query' => $search_tax,
    'posts_per_page' => 10
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('list_news','action_news',10,2);
function action_taxo($taxo){
  $terms = get_terms(array(
    'taxonomy' => $taxo,
    'hide_empty' => false,
    'orderby' => 'ID',
    'order' => 'asc',
    'parent' => 0
  ));
  return $terms;
}
add_filter('list_taxo','action_taxo');
function action_topics($name ='',$get){
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $search_tax = array();
  if($name) {
    $tax = array(
      'taxonomy' => 'cat_topic',
      'field' => 'slug',
      'terms' => $name
    );
    array_push($search_tax,$tax);
  }
  $args = array(
    'post_type' => 'topics',
    'orderby'   => 'id',
    'order'     => 'desc',
    'paged' => $paged,
    'tax_query' => $search_tax,
    'posts_per_page' => 9
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('list_topics','action_topics',10,2);
function action_partner($get){
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $search_tax = array();
  $args = array(
    'post_type' => 'partner',
    'orderby'   => 'id',
    'order'     => 'desc',
    'paged' => $paged,
    'posts_per_page' => 9
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('list_partner','action_partner',10,1);
function action_event($name ='',$get){
  if(!$get) {
    $paged = 1;
  } else {
    if(array_key_exists('pg',$get)){
      if($get['pg']) {
        $paged = $get['pg'];
      } else {
        $paged = 1;
      }
    } else {
      $paged = 1;
    }
    // $paged = ($get['pg']) ? $get['pg']: 1;
  }
  $search_tax = array();
  if($name) {
    $tax = array(
      'taxonomy' => 'cat_topic',
      'field' => 'slug',
      'terms' => $name
    );
    array_push($search_tax,$tax);
  }
  $args = array(
    'post_type' => 'event',
    'orderby'   => 'id',
    'order'     => 'desc',
    'paged' => $paged,
    'tax_query' => $search_tax,
    'posts_per_page' => 6
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('list_event','action_event',10,2);
function flash( $name = '', $message = '',$class = 'frm_error') {
  //We can only do something if the name isn't empty
  if(!empty($name)) {
    //No message, create it
    if(!empty( $message ) && empty( $_SESSION[$name])){
      if( !empty( $_SESSION[$name] ) ){
        unset( $_SESSION[$name] );
      }
      if( !empty( $_SESSION[$name.'_class'] ) ){
        unset( $_SESSION[$name.'_class'] );
      }
      $_SESSION[$name] = $message;
      $_SESSION[$name.'_class'] = $class;
    }
    //Message exists, display it
    elseif( !empty( $_SESSION[$name] ) && empty( $message ) ){
      echo $_SESSION[$name];
      unset($_SESSION[$name]);
      unset($_SESSION[$name.'_class']);
    }
  }
}
function handle_contact($post){
  $check = array();
  $mail = '';
  if($post) {
    foreach($post as $key => $value) {
      if($key != 'txt_other') {
        if(!$value) {
          array_push($check,$key);
          flash($key,'<p class="txt_error">必須項目です</p>');
          flash($key.'_class','frm_error');
        }
        if($key == 'txt_mail' && $value) {
          $mail = $value;
        }
        if($key == 'txt_mail_confirm' && $value != $mail) {
          array_push($check,$key);
          flash($key,'<p class="txt_error">必須項目です</p>');
          flash($key_class,'frm_error');
        }
        if($value) {
          flash($key.'_value',$value);
        }
      }
    }
    if(count($check) > 0) {
      wp_safe_redirect(get_bloginfo('url').'/contact');
      exit;
    }
  }
}
add_filter('check_contact','handle_contact',10,1);
function widget() {
  register_sidebar(array(
    'name' => __('Calendar', 'Calendar'),
    'id' => 'calendar',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
  ));
}
add_action('init', 'widget');
?>