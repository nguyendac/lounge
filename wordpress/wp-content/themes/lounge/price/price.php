<?php
/*
  Template Name: Price Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_access">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/price/images/bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/price/images/bkg_ttl_pc.jpg?v=4460752ca1ecf4b3a54e8ceb39ef6b3a" alt="Price 01" />
            </picture>
            <h2>PRICE<span>利用料金・利用規約</span></h2>
          </div>
        </div>
        <!--/.ttl-->
        <div class="gr_price">
          <div class="row">
            <p class="txt_top">Visions LOUNGEは学生と若手社会人のためのスペースとなっております。<br>大学生・25歳までの社会人はいつでも無料でラウンジのスペースと下記がご利用いただけます。</p>
            <div class="bx_list">
              <ul class="list_price">
                <li class="effect fadeIn delay_03">
                  <a href="#">
                    <figure>
                      <figcaption>
                        <span>FREE</span>
                        <em>Wi-Fi</em>
                      </figcaption>
                      <img src="<?php bloginfo('template_url')?>/price/images/icon_wf.png?v=3f21e6dad9b8e7b9bea9b4d226a67f14" alt="Icon 01">
                    </figure>
                  </a>
                </li>
                <li class="effect fadeIn delay_06">
                  <a href="#">
                    <figure>
                      <figcaption>
                        <span>FREE</span>
                        <em>電源</em>
                      </figcaption>
                      <img src="<?php bloginfo('template_url')?>/price/images/icon_d.png?v=aa7da4822094fda3a3a5cffb778ac4a9" alt="Icon 02">
                    </figure>
                  </a>
                </li>
                <li class="effect fadeIn delay_09">
                  <a href="#">
                    <figure>
                      <figcaption>
                        <span>FREE</span>
                        <em>ドリンク</em>
                      </figcaption>
                      <img src="<?php bloginfo('template_url')?>/price/images/icon_r.png?v=7aed27282137d7433a3766579574bed6" alt="Icon 03">
                    </figure>
                  </a>
                </li>
              </ul>
              <!--/./list_price-->
            </div>
            <!--/.bx_list-->
            <article>
              <h3 class="show_pc">初回ご利用時に、webでの簡単な会員登録が必要です。</h3>
              <p><span class="show_sp">初回ご利用時に、webでの簡単な会員登録が必要です。</span>２回目以降は、入店時に会員マイページを見せていただくことでご利用いただけます。<br>また、学生団体登録もご用意しており、登録いただいた団体にはスペースレンタルを無料でおこなっています。<br class="show_pc">（学生団体登録には事前審査がございます。ご希望の方は問い合わせフォームよりご連絡ください）</p>
              <figure class="effect maskToRight">
                <img src="<?php bloginfo('template_url')?>/price/images/img_p.jpg?v=3b6843a9533a782da177db7b3b240391" alt="images price">
              </figure>
            </article>
            <div class="bx_bottom">
              <h3>ご利用にあたっての各種規約は<br class="show_sp">以下よりご確認ください。</h3>
              <ul class="list_pdf">
                <li><a href="<?php bloginfo('template_url')?>/pdf/b.pdf" target="_blank"><span>会員利用規約(PDF)</span></a></li>
                <li><a href="<?php bloginfo('template_url')?>/pdf/a.pdf" target="_blank"><span>学生団体利用規約(PDF)</span></a></li>
                <li><a href="<?php bloginfo('template_url')?>/pdf/space_kiyaku.pdf" target="_blank"><span>スペースレンタル利用規約(PDF)</span></a></li>
              </ul>
              <!--/.list_pdf-->
            </div>
            <!--/.bx_bottom-->
          </div>
        </div>
        <!--/.gr_price-->
      </section>
      <!--/.st_access-->
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>

</body>
</html>
