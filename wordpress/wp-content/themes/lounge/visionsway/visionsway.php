<?php
/*
  Template Name: Visions Way Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="visions">
        <h2><em class="effect maskToRight delay_3"><span>志あふれる大人の生き方を学び、</span></em><br><em class="effect maskToRight delay_3"><span>自分が生きる未来を見つける。</span></em></h2>
        <figure class="effect maskToRight delay_6">
          <img src="<?php bloginfo('template_url')?>/visionsway/images/logo_visions.png?v=cd6981cdbbc8b6fa616eded3f0e89628" alt="Visions">
        </figure>
        <hr class="effect fadeIn delay_06">
        <h3 class="effect fadeIn delay_06">どんな仕事をして、どんな生き方をするか。<br>どんな人生だったらとびきり幸せなのか。</h3>
        <p class="effect fadeIn delay_06">あなたには明確なビジョンがありますか？<br class="show_pc">ただ食べるために働くのではなく自分だけの存在意義を見つけて、自分にしかできないこと、<br class="show_pc">自分だからこそ成し遂げられることを思い描いて、誰かに、何かに貢献して生きていく。<br>もしそんな生き方ができたら、かけがえのないファンに自然と囲まれるに違いない。<br class="show_pc">とびきり幸せな人生とはそういうものではないかと、私たちは考えます。</p>
        <p class="effect fadeIn delay_06">実際、世の中にはそんなかっこいい生き方をしている経営者、社会人がたくさんいます。<br>でも彼らも皆、若い頃は未来に不安を抱えていました。どんな人生経験を経て、自分らしさを磨き、<br class="show_pc">自分ならではの志や使命を見つけ、唯一無二の魅力を持った人になっていったのか？</p>
        <p class="effect fadeIn delay_06">彼らの生きざまを垣間見ながら、自分自身と重ね合わせ、未来像を少しずつはっきりとさせていく。<br class="show_pc"><span>彼らの人生やそこにある価値観にたくさんふれることができたら</span><br class="show_pc"><span>自分自身のビジョンを明確にするヒントになるのではないか。</span><br class="show_pc">”Visions Way”が生まれたきっかけはそこにあります。</p>
        <picture class="effect maskToRight">
          <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/visionsway/images/img_visions_sp.jpg" />
          <img src="<?php bloginfo('template_url')?>/visionsway/images/img_visions.jpg?v=6c9ab7df19492fb8b97583660cd8940b" alt="Visions Image">
        </picture>
        <h3 class="effect fadeIn delay_06">志のある人たちと共に、楽しみながら、<br class="show_sp">自分の未来を創り出す。<br>そんな体験をいっしょにしませんか？</h3>
        <p class="effect fadeIn delay_06">Visions LOUNGEが厳選した独自の志を持った経営者や社会人の半生を学び、<br class="show_pc">自分のビジョンについて考え、語り合えるセッションを毎週開催。毎回ゲストは変わっていくので、常に新鮮な刺激を得られます。<br>またフラットに語り合う時間も用意されているので、世代や立場を超えて、<br class="show_pc">フランクに意見交換することができます。ときにはゲストの企業をテーマに、<br class="show_pc">モノやコトを企画する賞金付きのコンペティションも開かれます。<br class="show_pc">こちらもぜひチャレンジしてみてください。</p>
        <p class="effect fadeIn delay_06">より詳しい内容はオリエンテーションにて。<br>随時開催していますのでまずは気軽に足を運んでみてください。</p>
        <div class="btn_blue effect fadeIn delay_06">
          <a href="https://select-type.com/rsv/?id=I5IZCj85ZHE&c_id=45405&w_flg=1" target="_blank"><span>オリエンテーションに予約する</span></a>
        </div>
      </section>
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>

</body>
</html>
