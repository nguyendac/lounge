<?php
/*
  Template Name: Access Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_access">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/access/images/bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/access/images/bkg_ttl_pc.jpg?v=71b2712207d7a883761adaee04ae647f" alt="Price 01" />
            </picture>
            <h2>ACCESS<span>アクセス</span></h2>
          </div>
        </div>
        <!--/.ttl-->
        <div class="gr_access">
          <div class="gr_access_top">
            <div class="left">
              <div class="left_m row">
                <div class="left_m_w">
                  <div class="top_h">
                    <h3>Visions LOUNGE Umeda</h3>
                    <span>阪急梅田駅直結、徒歩0分。</span>
                  </div>
                  <p>〒530-0012<br>大阪府大阪市北区芝田1-1-4 阪急ターミナルビル7F</p>
                  <p>TEL : <a class="tel" href="tel:06-6377-5646">06-6377-5646</a></p>
                  <div class="txt_bt">
                    <p>
                      阪急梅田駅「3F改札口」より徒歩1分<br>
                    地下鉄御堂筋線 梅田駅「北改札」より徒歩5分<br>
                    JR線 大阪駅「御堂筋口」より徒歩6分<br>
                    地下鉄谷町線 東梅田駅「北東改札」より徒歩7分<br>
                    阪神梅田駅 「東口」より徒歩8分
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <!--/.left-->
            <div class="right">
              <div class="map" id="map">map</div>
            </div>
            <!--/.right-->
          </div>
          <!--/.top-->
          <div class="row wrap">
            <div class="gr_access_bottom">
              <ul>
                <li class="effect fadeIn delay_03"><a href="#"><img src="<?php bloginfo('template_url')?>/access/images/img_01.jpg" alt="Images 01"></a></li>
                <li class="effect fadeIn delay_06"><a href="#"><img src="<?php bloginfo('template_url')?>/access/images/img_02.jpg" alt="Images 01"></a></li>
                <li class="effect fadeIn delay_09"><a href="#"><img src="<?php bloginfo('template_url')?>/access/images/img_03.jpg" alt="Images 01"></a></li>
              </ul>
            </div>
            <!--/.bottom-->
          </div>
        </div>
        <!--/.gr_access-->
      </section>
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3sl4xld55qkjnuWOIzUkpFgBB2caWsxI&callback=initMap"></script>
</body>
</html>
