function initMap() {
  var uluru = { lat: 34.704252, lng: 135.498445 };
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: uluru,
    panControl: false,
    scrollwheel: false,
    zoomControl: true,
    mapTypeId: 'roadmap',
    disableDefaultUI: true,
    styles: [{
        "stylers": [
          { "hue": "#858585" },
          { "saturation": -100 },
        ],
        "elementType": "all",
        "featureType": "all"
      },
      {
        "stylers": [{ "color": "#858585" }],
        "featureType": "road.highway",
      }
    ]
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map

  });
  google.maps.event.addDomListener(window, 'resize', function() {
    var center = map.getCenter()
    google.maps.event.trigger(map, "resize")
    map.setCenter(center)
  });
}
