<?php
/*
  Template Name: Event Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="event">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/event/images/event_banner.jpg" />
              <img src="<?php bloginfo('template_url')?>/event/images/event_banner.jpg?v=6d36289208201a35b704a785f81d11e9" alt="Events banner" />
            </picture>
            <h2>EVENT&<br>SCHEDULE<span>イベントスケジュール</span></h2>
          </div>
        </div>
        <div class="event_ct">
          <div class="event_ct_des">
            <div class="row">
              <h3>UP COMING EVENT</h3>
              <p>Visions LOUNGEでは、人生をかっこよく生きる人々の価値観・発想にふれることができるイベントを開催しています。</p>
            </div>
          </div>
          <div class="event_list">
            <div class="row">
              <div class="event_list_box">
                <?php
                  $events = apply_filters('list_event','',$_GET);
                  while ($events->have_posts()) : $events->the_post();
                  $terms = wp_get_post_terms($post->ID,'tag_event',array("fields" => "all"));
                ?>
                <div class="event_box effect fadeInUp">
                  <a href="<?php the_permalink();?>">
                    <div class="event_box_des">
                      <figure>
                        <?php
                        $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                        if(get_post_meta($post->ID,'thumb',true)) {
                          $img = get_post_meta($post->ID,'thumb',true);
                          $thumb = $img['url'];
                        }
                        ?>
                        <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                      </figure>
                      <?php if(get_post_meta($post->ID,'date_event',true)):?>
                      <div class="event_tag">
                        <span>
                          <?php 
                            $date = get_post_meta($post->ID,'date_event',true);
                          ?>
                          <sub><?php echo date('m',strtotime($date)) ?></sub>
                          <em><?php echo date('d',strtotime($date)) ?></em>
                        </span>
                        <small><?php echo substr(date('l',strtotime($date)),0,3) ?></small>
                      </div>
                    <?php endif;?>
                      <p><?php the_title()?></p>
                    </div>
                    <div class="event_box_link">
                      <?php foreach($terms as $term):?>
                      <span><?php _e($term->name)?></span>
                      <?php endforeach;?>
                    </div>
                  </a>
                </div>
              <?php endwhile;?>
              </div>
              <div class="bx_pag">
                <div class="bx_pag_l">
                  <?php
                  mp_pagination($prev = 'PREV', $next = 'NEXT', $pages=$news->max_num_pages);
                  wp_reset_query();
                  ?>
                </div>
              </div>
            </div>
          </div>
          <div class="event_schedule">
            <div class="row">
              <h3>SCHEDULE</h3>
              <?php
                $wrap_calendar = get_bloginfo('template_url').'/gcalendar-wrapper.php';
              ?>
              <iframe src="<?php _e($wrap_calendar) ?>?title=Visions%20LOUNGE&amp;showTitle=0&amp;showDate=0&amp;showPrint=0&amp;showCalendars=0&amp;showTz=0&amp;mode=WEEK&amp;height=600&amp;wkst=2&amp;hl=ja&amp;bgcolor=%23FFFFFF&amp;src=8lm9iqij1u4109tc7lhk2qloac%40group.calendar.google.com&amp;color=%232F6309&amp;src=gmqsiv0ab5fldl3nfis6untu7s%40group.calendar.google.com&amp;color=%23AB8B00&amp;ctz=Asia%2FTokyo" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
              <p class="explain"><span class="blue">ラウンジスペースの予定（営業時間・イベント・貸切）</span><span class="yellow">会議室の予定</span></p>
            </div>
          </div>
        </div>
      </section>
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer()?>

</body>
</html>
