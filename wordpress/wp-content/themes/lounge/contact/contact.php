<?php
/*
  Template Name: Contact Page
 */
 if( !session_id()){
   session_start();
 }
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_contact">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/contact/images/bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/contact/images/bkg_ttl_pc.jpg?v=1f764b9ada4813b05491d3aef9ceba9c" alt="Price 01" />
            </picture>
            <h2>CONTACT<span>お問い合わせ</span></h2>
          </div>
        </div>
        <!--/.ttl-->
        <div class="gr_contact">
          <div class="row wrap">
            <div class="ctn_form">
              <h3>問い合わせ内容をご入力ください。</h3>
              <p>スペースレンタルの場合はレンタル希望日時を記載いただくと仮予約が可能です。（営業カレンダーにて事前に空き状況をご確認ください）スペースレンタル可能時間は午前10:30〜午後21:30となります。
                スペースの内覧希望の方は、仮予約時にその旨をご記入ください。クルーの応募は<a href="https://www.wantedly.com/companies/company_2731290" target="_blank">wantedly</a>より受け付けております。</p>
                <ul class="list_item">
                  <li class="active">入力</li>
                  <li>確認</li>
                  <li>完了</li>
                </ul>
                <!--/.list_item-->
                <?php

                ?>
                <form class="frm_contact" action="<?php _e(home_url())?>/contact/confirm" method="post">
                  <div class="frm_group">
                    <label><em>氏名</em><span class="req">必須</span></label>
                    <div class="left">
                      <input type="text" name="txt_name" id="idName" class="frm_control <?php flash('txt_name_class')?>" placeholder="例 ）山田　太郎" value="<?php flash('txt_name_value')?>">
                      <?php flash('txt_name')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_group">
                    <label><em>フリガナ</em><span class="req">必須</span></label>
                    <div class="left">
                      <input type="text" name="txt_phonetic" id="idPhonetic" class="frm_control <?php flash('txt_phonetic_class')?>" placeholder="例 ）ヤマダタロウ" value="<?php flash('txt_phonetic_value')?>">
                      <?php flash('txt_phonetic')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_group">
                    <label><em>企業名・学校名</em><span class="req">必須</span></label>
                    <div class="left">
                      <input type="text" name="txt_company" id="idCompany" class="frm_control <?php flash('txt_company_class')?>" placeholder="例 ）ヤマダ学校" value="<?php flash('txt_company_value')?>">
                      <?php flash('txt_company')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_group">
                    <label><em>メールアドレス</em><span class="req">必須</span></label>
                    <div class="left">
                      <input type="mail" class="frm_control <?php flash('txt_mail_class')?>" name="txt_mail" id="idMail" placeholder="例 ）example@mail.co.jp" value="<?php flash('txt_mail_value')?>">
                      <?php flash('txt_mail')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_group">
                    <label><em>メールアドレス(確認)</em><span class="req">必須</span></label>
                    <div class="left">
                      <input type="mail" class="frm_control <?php flash('txt_mail_confirm_class')?>" name="txt_mail_confirm" id="idMailConfirm" placeholder="例 ）example@mail.co.jp" value="<?php flash('txt_mail_confirm_value')?>">
                      <?php flash('txt_mail_confirm')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_group">
                    <label><em>電話番号</em><span class="req">必須</span></label>
                    <div class="left">
                      <input type="text" class="frm_control <?php flash('txt_tel_class')?>" name="txt_tel" id="idTel" placeholder="例 ）03-5651-7544" value="<?php flash('txt_tel_value')?>">
                      <?php flash('txt_tel')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_group">
                    <label><em>お問い合わせ内容</em><span class="req">必須</span></label>
                    <div class="left">
                      <select name="slt_inquiry" id="idInquiry" class="frm_select <?php flash('slt_inquiry_class')?>">
                        <option value="パートナー企業希望">パートナー企業希望</option>
                        <option value="ラウンジの利用（会員登録のご質問）">ラウンジの利用（会員登録のご質問）</option>
                        <option value="ラウンジの利用（学生団体登録のご質問）">ラウンジの利用（学生団体登録のご質問）</option>
                        <option value="スペースレンタルの仮予約希望">スペースレンタルの仮予約希望</option>
                        <option value="その他">その他</option>
                      </select>
                      <?php flash('slt_inquiry')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_group">
                    <label><em>その他</em><span class="blue">任意</span></label>
                    <div class="left">
                      <textarea class="frm_textarea <?php flash('txt_other_class')?>" name="txt_other" id="idOther"><?php flash('txt_other_value')?></textarea>
                      <?php flash('txt_other')?>
                    </div>
                  </div>
                  <!--/.frm_group-->
                  <div class="frm_btn">
                    <button class="btn_hv" type="submit" name="check" value="1"><span>入力内容を確認する</span></button>
                  </div>
                </form>
            </div>
            <!--/.ctn_form-->
          </div>
        </div>
        <!--/.gr_contact-->
      </section>
      <!--/.st_contact-->
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>

</body>
</html>
