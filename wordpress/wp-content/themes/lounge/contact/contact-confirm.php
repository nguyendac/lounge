<?php
/*
  Template Name: Contact Confirm Page
 */
 if( !session_id()){
   session_start();
 }
 if(isset($_POST['check'])) {
   $checks = apply_filters('check_contact',$_POST);
 } else {
   wp_safe_redirect(get_bloginfo('url').'/contact');
   exit;
 }
 get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_contact">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/contact/images/bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/contact/images/bkg_ttl_pc.jpg?v=1f764b9ada4813b05491d3aef9ceba9c" alt="Price 01" />
            </picture>
            <h2>CONTACT<span>お問い合わせ</span></h2>
          </div>
        </div>
        <!--/.ttl-->
        <div class="gr_contact gr_confirm">
          <div class="row wrap">
            <div class="ctn_form">
              <h3>問い合わせ内容をご確認ください。</h3>
              <ul class="list_item">
                <li>入力</li>
                <li class="active">確認</li>
                <li>完了</li>
              </ul>
              <!--/.list_item-->
              <form class="frm_contact frm_confirm" action="<?php _e(home_url())?>/contact/thanks" method="post">
                <div class="frm_group">
                  <label><em>氏名</em></label>
                  <div class="left">
                    <input type="hidden" name="txt_name" value="<?php _e($_POST['txt_name'])?>">
                    <p><?php _e($_POST['txt_name'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_group">
                  <label><em>フリガナ</em></label>
                  <div class="left">
                    <input type="hidden" name="txt_phonetic" value="<?php _e($_POST['txt_phonetic'])?>">
                    <p><?php _e($_POST['txt_phonetic'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_group">
                  <label><em>企業名・学校名</em></label>
                  <div class="left">
                    <input type="hidden" name="txt_company" value="<?php _e($_POST['txt_company'])?>">
                    <p><?php _e($_POST['txt_company'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_group">
                  <label><em>メールアドレス</em></label>
                  <div class="left">
                    <input type="hidden" name="txt_mail" value="<?php _e($_POST['txt_mail'])?>">
                    <p><?php _e($_POST['txt_mail'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_group">
                  <label><em>メールアドレス(確認)</em></label>
                  <div class="left">
                    <input type="hidden" name="txt_mail_confirm" value="<?php _e($_POST['txt_mail_confirm'])?>">
                    <p><?php _e($_POST['txt_mail_confirm'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_group">
                  <label><em>電話番号</em></label>
                  <div class="left">
                    <input type="hidden" name="txt_tel" value="<?php _e($_POST['txt_tel'])?>">
                    <p><?php _e($_POST['txt_tel'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_group">
                  <label><em>お問い合わせ内容</em></label>
                  <div class="left">
                    <input type="hidden" name="slt_inquiry" value="<?php _e($_POST['slt_inquiry'])?>">
                    <p><?php _e($_POST['slt_inquiry'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_group">
                  <label><em>その他</em></label>
                  <div class="left">
                    <input type="hidden" name="txt_other" value="<?php _e($_POST['txt_other'])?>">
                    <p><?php _e($_POST['txt_other'])?></p>
                  </div>
                </div>
                <!--/.frm_group-->
                <div class="frm_btn">
                  <button type="button" id="btnBack" class="back_top btn_hv" onclick="window.history.back();"><span>修正する</span></button>
                  <button class="btn_hv" type="submit" name="thanks" value="thanks"><span>送信する</span></button>
                </div>
              </form>
            </div>
            <!--/.ctn_form-->
          </div>
        </div>
        <!--/.gr_contact-->
      </section>
      <!--/.st_contact-->
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer();?>

</body>
</html>
