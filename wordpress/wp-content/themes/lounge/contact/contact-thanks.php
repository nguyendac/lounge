<?php
/*
  Template Name: Contact Thanks Page
 */
 if( !session_id()){
   session_start();
 }
 if(isset($_POST['thanks'])) {
   // $option =($_POST['option']) ? $_POST['option'] : '';
   $name = ($_POST['txt_name']) ? $_POST['txt_name'] : '';
   $phonetic = ($_POST['txt_phonetic']) ? $_POST['txt_phonetic'] : '';
   $company = ($_POST['txt_company']) ? $_POST['txt_company'] : '';
   $email = ($_POST['txt_mail']) ? $_POST['txt_mail'] : '';
   $phone = ($_POST['txt_tel']) ? $_POST['txt_tel'] : '';
   $inquiry = ($_POST['slt_inquiry']) ? $_POST['slt_inquiry'] : '';
   $content = ($_POST['txt_other']) ? $_POST['txt_other'] : '';
   $to = 'daccuongdn@gmail.com';
   // Always set content-type when sending HTML email
   $subject = "【ラウンジ問い合わせ】";
   $message_admin = "<br>";
   // $message_admin .= "株式会社オーセンのコーポレートサイトから、<br>";
   // $message_admin .= "以下の内容にてお問い合わせがありました。";
   // $message_admin .= "<br><br>";
   $message_admin .= "----------------------------<br>";
   $message_admin .= "お問い合わせ項目：".$inquiry."<br>";
   $message_admin .= "<br>";
   $message_admin .= "お名前：".$name."<br>";
   $message_admin .= "<br>";
   $message_admin .= "メールアドレス：".$email."<br>";
   $message_admin .= "<br>";
   $message_admin .= "電話番号：".$phone."<br>";
   $message_admin .= "<br>";
   $message_admin .= "会社名：".$company."<br>";
   $message_admin .= "<br>";
   $message_admin .= "お問い合わせ内容：".$content."<br>";
   $message_admin .= "<br>";
   $message_admin .= "----------------------------";
   $headers =  'MIME-Version: 1.0' . "\r\n";
   $headers .= 'From: Lounge Vision <info.visionslounge@prdx.co.jp>' . "\r\n";
   $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
   // send mail to admin
   mail($to, $subject, $message_admin, $headers);
   // send mail to client
   $subject_client = '【Visions LOUNGE】お問い合わせを受付ました';
   $message_client =  "<br>";
   $message_client .= 'お問い合わせありがとうございました。<br>';
   $message_client .= '確認後、担当者よりご連絡させていただきますので<br>';
   $message_client .= '少々お待ちくださいませ。<br>';
   $message_client .= 'よろしくお願いいたします<br>';
   mail($email, $subject_client, $message_client, $headers);

 } else {
   wp_safe_redirect(get_bloginfo('url').'/contact');
   exit;
 }
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="st_contact">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/contact/images/bkg_ttl_sp.png" />
              <img src="<?php bloginfo('template_url')?>/contact/images/bkg_ttl_pc.jpg?v=1f764b9ada4813b05491d3aef9ceba9c" alt="Price 01" />
            </picture>
            <h2>CONTACT<span>お問い合わせ</span></h2>
          </div>
        </div>
        <!--/.ttl-->
        <div class="gr_contact gr_thanks">
          <div class="row wrap">
            <div class="ctn_form">
              <h3>問い合わせありがとうございました。</h3>
              <ul class="list_item">
                <li>入力</li>
                <li>確認</li>
                <li class="active">完了</li>
              </ul>
              <!--/.list_item-->
              <div class="bx_thanks">
                <p>お問い合わせメールが正常に送信されました。</p>
                <p>ありがとうございました。<br>確認次第担当者よりご連絡させていただきますので、いましばらくお待ちください。</p>
                <a class="back_top btn_hv" href="<?php _e(home_url())?>"><span>TOP</span></a>
              </div>
              <!--/.bx_thanks-->
            </div>
            <!--/.ctn_form-->
          </div>
        </div>
        <!--/.gr_contact-->
      </section>
      <!--/.st_contact-->
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>

  </body>
  </html>
