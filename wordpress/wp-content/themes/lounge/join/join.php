<?php
/*
  Template Name: Join Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="join">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/join/images/partner_banner.jpg" />
              <img src="<?php bloginfo('template_url')?>/join/images/join_banner.jpg?v=9e15bab1863681b5257ecb618b95285a" alt="Join banner" />
            </picture>
            <h2>JOIN<span>パートナー・クルー募集</span></h2>
          </div>
        </div>
        <div class="join_ct">
          <div class="row">
            <div class="join_ct_inner">
              <div class="join_ct_des">
                <h3 class="show_pc">パートナーご希望の企業様へ</h3>
                <p>Visions LOUNGEでは、ビジョンを語り合う場所を共につくっていくパートナー企業を募集しています。パートナープランについては下記よりお問い合わせください。</p>
                <div class="bx_pag btn_contact">
                  <a class="btn_hv" href="/contact/"><span>CONTACT</span></a>
                </div>
              </div>
              <div class="join_ct_thumb">
                <h3 class="show_sp">パートナーご希望の企業様へ</h3>
                <figure class="effect maskToRight">
                  <img src="<?php bloginfo('template_url')?>/join/images/join_img1.jpg?v=35b1fd8396089ce4056790abe83909be" alt="Join image" />
                </figure>
              </div>
            </div>
            <div class="join_ct_inner">
              <div class="join_ct_des">
                <h3 class="show_pc">学生クルーご希望の皆様へ</h3>
                <p>Visions LOUNGEでは、この場で自分を磨きたいという学生たちがクルーとして日々運営に携わっています。イベントの企画や実施、店舗づくりなど自分次第で様々なことに挑戦できます。詳細は下記にてご覧ください。</p>
                <div class="bx_pag btn_wanted">
                  <a class="btn_hv" href="https://www.wantedly.com/companies/company_2731290" target="_blank"><span>WANTEDLY</span></a>
                </div>
              </div>
              <div class="join_ct_thumb">
                <h3 class="show_sp">学生クルーご希望の皆様へ</h3>
                <figure class="effect maskToRight">
                  <img src="<?php bloginfo('template_url')?>/join/images/join_img2.jpg?v=937e91d5b143304a758c38555872d9cf" alt="Join image" />
                </figure>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer()?>
</body>
</html>
