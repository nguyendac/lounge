<?php
/*
  Template Name: Partner Page
 */
get_header();
?>

<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/template','header')?>
    </header>
    <main class="main">
      <section class="partner">
        <div class="ttl">
          <div class="row">
            <picture class="effect maskToRight">
              <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/partner/images/partner_banner.jpg" />
              <img src="<?php bloginfo('template_url')?>/partner/images/partner_banner.jpg?v=0fe971dedef82d1b4d3d9226e4217bc3" alt="partner banner" />
            </picture>
            <h2>PARTNER<span>パートナー</span></h2>
          </div>
        </div>
        <div class="partner_ct">
          <div class="partner_ct_des">
            <div class="row">
              <p>Visions LOUNGEの考え方に共感し協力していただいているパートナー企業をご紹介します。<br>共にイベントの企画・実施をおこなっています。</p>
            </div>
          </div>
          <div class="partner_list">
            <div class="row">
              <div class="partner_list_box">
                <?php
                  $partners = apply_filters('list_partner',$_GET);
                  while ($partners->have_posts()) : $partners->the_post();
                ?>
                <div class="partner_box">
                  <?php if(get_post_meta($post->ID,'link',true)):?>
                    <a href="<?php _e(get_post_meta($post->ID,'link',true))?>" target="_blank">
                  <?php else:?>
                    <a href="javascript::void(0)">
                  <?php endif;?>
                    <figure>
                      <?php
                      $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                      if(get_post_meta($post->ID,'logo',true)) {
                        $img = get_post_meta($post->ID,'logo',true);
                        $thumb = $img['url'];
                      }
                      ?>
                      <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                    </figure>
                    <div class="partner_box_des">
                      <p><?php _e(get_post_meta($post->ID,'desc',true))?></p>
                      <div class="btn_media">
                        <?php if(get_post_meta($post->ID,'link',true)): ?>
                        <span>MEDIA</span>
                      <?php endif?>
                      </div>
                    </div>
                  </a>
                </div>
                <?php endwhile;wp_reset_query();?>
              </div>
              <div class="bx_pag">
                <div class="bx_pag_l">
                  <?php
                  mp_pagination($prev = 'PREV', $next = 'NEXT', $pages=$partners->max_num_pages);
                  wp_reset_query();
                  ?>
                </div>
              </div>
              <div class="btn_green">
                <a href="/join/"><span>パートナーご希望の企業様</span></a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>

    <footer id="footer" class="footer">
      <?php get_template_part('templates/template','footer')?>
    </footer>
  </div>
<?php get_footer()?>
</body>
</html>
