<?php
get_header();
?>
<body>
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div id="container" class="container">
      <header id="header" class="header">
        <?php get_template_part('templates/template','header')?>
      </header>
      <main class="main">
        <section class="topics_detail">
          <figure class="banner_detail">
            <?php 
            $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
            if(get_post_meta(get_the_ID(),'thumb',true)) {
              $img = get_post_meta(get_the_ID(),'thumb',true);
              $thumb = $img['url'];
            }
            ?>
            <img src="<?php _e($thumb)?>" alt="Topics banner" />
          </figure>
          <div class="row">
            <div class="topics_ct">
              <div class="topics_box_des">
                <?php 
                  $term = wp_get_post_terms(get_the_ID(),'cat_topic',array("fields" => "all"));
                  $cat_name = 'No Cate';
                  if(count($term)) {
                    $cat_name = $term[0]->name;
                  }
                ?>
                <?php if(strtolower($cat_name) == 'blog') :?>
                  <em class="topics_box_cat blog">BLOG</em>
                <?php endif;?>
                <?php if(strtolower($cat_name) == 'media') :?>
                  <em class="topics_box_cat media">MEDIA</em>
                <?php endif;?>
                <time datetime="<?php _e(get_the_date('Y-m-d'))?>"><?php _e(get_the_date('Y.m.d'))?></time>
              </div>
              <h1><?php the_title()?></h1>
              <?php _e(nl2br(the_content()))?>
            </div>
            <div class="other_post">
              <h3 class="other_post_tt">OTHER POST</h3>
              <div class="other_post_inner">
                <?php 
                  $topics = apply_filters('get_3_topics','');
                  while ($topics->have_posts()) : $topics->the_post();
                  $term = wp_get_post_terms($post->ID,'cat_topic',array("fields" => "all"));
                  $cat_name = 'No Cate';
                  if(count($term)) {
                    $cat_name = $term[0]->name;
                  }
                  if(strtolower($cat_name) === 'media') {
                    $target = true;
                  } else {
                    $target = false;
                  }
                ?>
                <div class="topics_box effect fadeInUp">
                    <?php if($target) : ?>
                      <a href="<?php _e(get_post_meta($post->ID,'link',true))?>" target="_blank">
                    <?php else:?>
                      <a href="<?php the_permalink()?>">
                    <?php endif;?>
                    <figure>
                      <?php 
                        $thumb = get_bloginfo('template_url')."/common/images/noimage.jpg";
                        if(get_post_meta($post->ID,'thumb',true)) {
                          $img = get_post_meta($post->ID,'thumb',true);
                          $thumb = $img['url'];
                        }
                      ?>
                      <img src="<?php _e($thumb)?>" alt="<?php the_title()?>" />
                    </figure>
                    <div class="topics_box_des">
                      <?php if($target):?>
                        <em class="topics_box_cat media">MEDIA</em>
                      <?php else:?>
                        <em class="topics_box_cat blog"><?php _e($cat_name)?></em>
                      <?php endif;?>
                      
                      <p><?php the_title();?></p>
                      <div class="topics_box_link">
                        <time datetime="<?php the_time('Y-m-d')?>"><?php the_time('Y.m.d')?></time>
                        <?php if($target):?>
                          <span>外部リンクへ</span>
                        <?php endif;?>
                      </div>
                    </div>
                  </a>
                </div>
              <?php endwhile;?>
              </div>
              <div class="bx_pag pag_detail">
                <a class="btn_hv" href="/topics"><span>BACK TO LIST</span></a>
                <!--/.list_pag-->
              </div>
            </div>
          </div>
        </section>
      </main>
      <footer id="footer" class="footer">
        <?php get_template_part('templates/template','footer')?>
      </footer>
    </div>
  <?php get_footer();?>
  <?php endwhile; endif; ?>
</body>
</html>