window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback){
    return window.setTimeout(callback, 1000/60);
  };
})();

window.cancelAnimFrame = (function(_id) {
  return window.cancelAnimationFrame ||
  window.cancelRequestAnimationFrame ||
  window.webkitCancelAnimationFrame ||
  window.webkitCancelRequestAnimationFrame ||
  window.mozCancelAnimationFrame ||
  window.mozCancelRequestAnimationFrame ||
  window.msCancelAnimationFrame ||
  window.msCancelRequestAnimationFrame ||
  window.oCancelAnimationFrame ||
  window.oCancelRequestAnimationFrame ||
  function(_id) { window.clearTimeout(id); };
})();
function closest(el,selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}
function getCssProperty(elem, property){
   return window.getComputedStyle(elem,null).getPropertyValue(property);
}
var easingEquations = {
  easeOutSine: function (pos) {
      return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function (pos) {
      return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function (pos) {
      if ((pos /= 0.5) < 1) {
          return 0.5 * Math.pow(pos, 5);
      }
      return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};
function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}
function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}
function CreateElementWithClass(elementName ,className) {
  var el = document.createElement(elementName);
  el.className = className;
  return el;
}
function createElementWithId(elementName,idName){
  var el =  document.createElement(elementName);
  el.id = idName;
  return el;
}
function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    document.body.appendChild(outer);
    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";
    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);
    var widthWithScroll = inner.offsetWidth;
    // remove divs
    outer.parentNode.removeChild(outer);
    return widthNoScroll - widthWithScroll;
}
var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
var flex = ['-webkit-box', '-moz-box', '-ms-flexbox', '-webkit-flex', 'flex'];
var fd = ['flexDirection', '-webkit-flexDirection', '-moz-flexDirection'];
var animatriondelay = ["animationDelay","-moz-animationDelay","-wekit-animationDelay"];
function getSupportedPropertyName(properties) {
  for (var i = 0; i < properties.length; i++) {
    if (typeof document.body.style[properties[i]] != "undefined") {
      return properties[i];
    }
  }
  return null;
}
var transformProperty = getSupportedPropertyName(transform);
var flexProperty = getSupportedPropertyName(flex);
var fdProperty = getSupportedPropertyName(fd);
var ad = getSupportedPropertyName(animatriondelay);
function detectIE(){
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  var trident = ua.indexOf('Trident/');
  if (msie > 0 || trident > 0) {
    // IE 10 or older => return version number
    // return 'ie'+parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    return 'ie';
  }
  return false;
}
function detect7(){
  var ua = window.navigator.userAgent;
  var isWin7 = ua.indexOf('Windows NT 6.1');
  if(isWin7 > 0) {
    return 'win7';
  }
  return false;
};
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
window.addEventListener('DOMContentLoaded', function() {
  if(detectIE()){
    document.body.classList.add(detectIE());
  }
  if(detect7()) {
    document.body.classList.add(detect7());
  }
 /* new ObjFit();*/
  // new ExtraNav();
  new MenuSp();
  new Sticky();
  new Effect();
  new FullEffect();
});
var MenuSp = (function(){
  function MenuSp(){
    var m = this;
    this._target = document.getElementById('open_nav');
    this._close = document.getElementById('close_nav');
    this._mobile = document.getElementById('nav');
    this._header = document.getElementById('header');
    this._target.addEventListener('click',function(){
      if(this.classList.contains('open')){
        this.classList.remove('open');
        m._mobile.classList.remove('open');
        document.body.style.overflow = 'inherit';
      } else {
        this.classList.add('open');
        m._mobile.classList.add('open');
        document.body.style.overflow = 'hidden';
      }
    })
    this._close.addEventListener('click',function(e){
      e.preventDefault();
      m._target.click();
    })
    this._reset = function(){
      if(window.innerWidth > 768) {
        m._target.classList.remove('open');
        m._mobile.classList.remove('open');
        document.body.style.overflow = 'inherit';
      }
    }
    this._reset();
    window.addEventListener('resize',m._reset,false);
  }
  return MenuSp;
})()
var Sticky = (function(){
  function Sticky(){
    var s = this;
    this._target = document.getElementById('header');
    this._mobile = document.getElementById('open_nav');
    this._for_sp = function(top){
      s._target.style.left = 0;
      document.body.style.paddingTop = s._target.clientHeight+'px';
      if(top > 0) {
        s._target.classList.add('fixed');
        document.body.style.paddingTop = s._target.clientHeight+'px';
      } else {
        s._target.classList.remove('fixed');
        document.body.style.paddingTop = 0;
      }
    }
    this._for_pc = function(top,left){
      if(top > 0) {
        s._target.classList.add('fixed');
        document.body.style.paddingTop = s._target.clientHeight+'px';
        s._target.style.left = -left+'px';
      } else {
        s._target.classList.remove('fixed');
        document.body.style.paddingTop = 0;
      }
    }
    this.handling = function(){
      var _top  = document.documentElement.scrollTop || document.body.scrollTop;
      var _left  = document.documentElement.scrollLeft || document.body.scrollLeft;
      if(window.innerWidth < 769) {
        s._for_sp(_top);
      }  else {
        if(!s._target.classList.contains('top')) {
          s._target.classList.remove('fixed')
        }
        s._for_pc(_top,_left);
      }
    }
    window.addEventListener('scroll',s.handling,false);
    window.addEventListener('resize',s.handling,false);
    window.addEventListener('load',s.handling,false);
  }
  return Sticky;
})();
var ObjFit = (function(){
  function ObjFit(){
    var o =  this;
    this._els = document.querySelectorAll('.wrap_obj');
    this._handling = function(){
      Array.prototype.forEach.call(o._els,function(el){
        var _src = el.querySelector('img').src;
        el.style.backgroundImage = "url("+_src+")";
      })
    }
    if(detectIE()) {
      o._handling();
    }
  }
  return ObjFit;
})()

/* images pc <---> sp */
(function () {
  var PicturePolyfill = (function () {
    function PicturePolyfill() {
      var _this = this;
      this.pictures = [];
      this.onResize = function () {
        var width = document.body.clientWidth;
        for (var i = 0; i < _this.pictures.length; i = (i + 1)) {
          _this.pictures[i].update(width);
        };
      };
      if ([].includes) return;
      var picture = Array.prototype.slice.call(document.getElementsByTagName('picture'));
      for (var i = 0; i < picture.length; i = (i + 1)) {
        this.pictures.push(new Picture(picture[i]));
      };
      window.addEventListener("resize", this.onResize, false);
      this.onResize();
    }
    return PicturePolyfill;
  }());
  var Picture = (function () {
    function Picture(node) {
      var _this = this;
      this.update = function (width) {
        width <= _this.breakPoint ? _this.toSP() : _this.toPC();
      };
      this.toSP = function () {
        if (_this.isSP) return;
        _this.isSP = true;
        _this.changeSrc();
      };
      this.toPC = function () {
        if (!_this.isSP) return;
        _this.isSP = false;
        _this.changeSrc();
      };
      this.changeSrc = function () {
        var toSrc = _this.isSP ? _this.srcSP : _this.srcPC;
        _this.img.setAttribute('src', toSrc);
      };
      this.img = node.getElementsByTagName('img')[0];
      this.srcPC = this.img.getAttribute('src');
      var source = node.getElementsByTagName('source')[0];
      this.srcSP = source.getAttribute('srcset');
      this.breakPoint = Number(source.getAttribute('media').match(/(\d+)px/)[1]);
      this.isSP = !document.body.clientWidth <= this.breakPoint;
      this.update();
    }
    return Picture;
  }());
  new PicturePolyfill();
}());
var Effect = (function(){
  function Effect(){
    var e = this;
    this.eles = document.querySelectorAll('.effect');
    this.handling = function(){
      var _top  = document.documentElement.scrollTop
      Array.prototype.forEach.call(e.eles,function(el,i){
        if(isPartiallyVisible(el)){
          el.classList.add('active');
        }
      })
    }
    window.addEventListener('scroll',e.handling,false);
    this.handling();
  }
  return Effect;
})();
var FullEffect = (function(){
  function FullEffect(){
    var e = this;
    this.eles = document.querySelectorAll('.full');
    this.handling = function(){
      if(window.innerWidth > 769) {
        Array.prototype.forEach.call(e.eles,function(el,i){
          if(isFullyVisible(el)){
            el.classList.add('active');
          }
        })
      } else {
        Array.prototype.forEach.call(e.eles,function(el,i){
          if(isPartiallyVisible(el)){
            el.classList.add('active');
          }
        })
      }
    }
    window.addEventListener('scroll',e.handling,false);
    this.handling();
  }
  return FullEffect;
})();